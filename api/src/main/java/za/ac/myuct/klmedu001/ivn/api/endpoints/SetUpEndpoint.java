package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.NotFoundException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import za.ac.myuct.klmedu001.ivn.api.Constants;
import za.ac.myuct.klmedu001.ivn.api.entity.Candidate;
import za.ac.myuct.klmedu001.ivn.api.entity.CandidateType;
import za.ac.myuct.klmedu001.ivn.api.entity.Country;
import za.ac.myuct.klmedu001.ivn.api.entity.District;
import za.ac.myuct.klmedu001.ivn.api.entity.LGA;
import za.ac.myuct.klmedu001.ivn.api.entity.LastUpdate;
import za.ac.myuct.klmedu001.ivn.api.entity.Party;
import za.ac.myuct.klmedu001.ivn.api.entity.Post;
import za.ac.myuct.klmedu001.ivn.api.entity.State;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Set Up Endpoint
 * TODO Lock this endpoint later
 */
@Api(
        name = "setUpApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class SetUpEndpoint implements Constants{
    private static final Logger logger = Logger.getLogger(SetUpEndpoint.class.getName());

    public List<LastUpdate> setUpEnv() throws NotFoundException {
        setupCountries();
        setupCandidates();
        setupCandidateTypes();
        setupParties();
        setupStates();
        setupDistricts();
        setupLGAs();

        return ofy().load().type(LastUpdate.class).orderKey(false).list();
    }

    /**
     * Sets up Datastore with Country info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupCountries",
        path = "setup/countries",
        httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupCountries() throws NotFoundException {
        logger.info("Setting up countries entity");

        File f = new File(FILEPATH_COUNTRY);
        List<Country> countries = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("countries.csv file not found");

        //Load last country inserted to get offset of new Id(s) to put in
        Country lastEntry = ofy().load().type(Country.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] countryRawEntry = str.split(",");
                Country entry = new Country(countryRawEntry[0], countryRawEntry[1]);
                entry.setId(newId);
                countries.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(countries).now();

        return updateLastUpdate(DATASTORE_COUNTRY);
    }

    /**
     * Sets up Datastore with {@link CandidateType} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupCandidateTypes",
            path = "setup/candidatetypes",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupCandidateTypes() throws NotFoundException {
        logger.info("Setting up candidate types entity");

        File f = new File(FILEPATH_CANDIDATE_TYPE);
        List<CandidateType> candidateTypes = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("candidate-types.csv file not found");

        //Load last candidate type inserted to get offset of new Id(s) to put in
        CandidateType lastEntry = ofy().load().type(CandidateType.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                CandidateType entry = new CandidateType(str);
                entry.setId(newId);
                candidateTypes.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(candidateTypes).now();

        return updateLastUpdate(DATASTORE_CANDIDATE_TYPE);
    }

    /**
     * Sets up Datastore with {@link Candidate} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupCandidates",
            path = "setup/candidates",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupCandidates() throws NotFoundException {
        logger.info("Setting up candidate entity");

        File f = new File(FILEPATH_CANDIDATE);
        List<Candidate> Candidates = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("candidate.csv file not found");

        //Load last party inserted to get offset of new Id(s) to put in
        Candidate lastEntry = ofy().load().type(Candidate.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] candidateRawEntry = str.split(",");
                Candidate entry = new Candidate(candidateRawEntry);
                entry.setId(newId);
                Candidates.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(Candidates).now();

        return updateLastUpdate(DATASTORE_CANDIDATE);
    }

    /**
     * Sets up Datastore with {@link Party} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupParties",
            path = "setup/Parties",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupParties() throws NotFoundException {
        logger.info("Setting up Party entity");

        File f = new File(FILEPATH_PARTY);
        List<Party> Parties = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("Party.csv file not found");

        //Load last party inserted to get offset of new Id(s) to put in
        Party lastEntry = ofy().load().type(Party.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] PartyRawEntry = str.split(",");
                Party entry = new Party(PartyRawEntry);
                entry.setId(newId);
                Parties.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(Parties).now();

        return updateLastUpdate(DATASTORE_PARTY);
    }

    /**
     * Sets up Datastore with {@link State} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupStates",
            path = "setup/States",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupStates() throws NotFoundException {
        logger.info("Setting up State entity");

        File f = new File(FILEPATH_STATE);
        List<State> States = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("State.csv file not found");

        //Load last state inserted to get offset of new Id(s) to put in
        State lastState = ofy().load().type(State.class).orderKey(true).first().now();
        long newId = 1;
        if(lastState != null)
            newId = lastState.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] StateRawEntry = str.split(",");
                State entry = new State(StateRawEntry[0], Long.parseLong(StateRawEntry[1]));
                entry.setId(newId);
                States.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(States).now();

        return updateLastUpdate(DATASTORE_STATE);
    }

    /**
     * Sets up Datastore with {@link District} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupDistricts",
            path = "setup/districts",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupDistricts() throws NotFoundException {
        logger.info("Setting up District entity");

        File f = new File(FILEPATH_DISTRICT);
        List<District> districts = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("District.csv file not found");

        //Load last lga inserted to get offset of new Id(s) to put in
        District lastEntry = ofy().load().type(District.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] DistrictRawEntry = str.split(",");

                District entry = new District(DistrictRawEntry[0], Long.parseLong(DistrictRawEntry[1]));
                entry.setId(newId);
                districts.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(districts).now();

        return updateLastUpdate(DATASTORE_DISTRICT);
    }

    /**
     * Sets up Datastore with {@link LGA} info
     * @return {@link LastUpdate} of the current Entity Type
     * @throws NotFoundException if required file is not found
     */
    @ApiMethod(name = "setupLGAs",
            path = "setup/LGAs",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupLGAs() throws NotFoundException {
        logger.info("Setting up LGA entity");

        File f = new File(FILEPATH_LGA);
        List<LGA> LGAs = new ArrayList<>(5);

        if(!f.exists())
            throw new NotFoundException("LGA.csv file not found");

        //Load last lga inserted to get offset of new Id(s) to put in
        LGA lastEntry = ofy().load().type(LGA.class).orderKey(true).first().now();
        long newId = 1;
        if(lastEntry != null)
            newId = lastEntry.getId()+1;

        try{
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str;
            while ((str = br.readLine()) != null){
                String [] LGARawEntry = str.split(",");
                LGA entry = new LGA(LGARawEntry[0].replace(';', ','), Long.parseLong(LGARawEntry[1]));
                entry.setId(newId);
                LGAs.add(checkIfExists(entry));
                ++newId;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ofy().save().entities(LGAs).now();

        return updateLastUpdate(DATASTORE_LGA);
    }

    /**
     * Sets up Datastore with {@link za.ac.myuct.klmedu001.ivn.api.entity.Post} info
     * @return {@link LastUpdate} of the current Entity Type
     */
    @ApiMethod(name = "setupPosts",
            path = "setup/posts",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate setupPosts(){
        List<Post> posts = new ArrayList<>();
        List<Party> parties = ofy().load().type(Party.class).orderKey(true).list();
        List<Candidate> candidates = ofy().load().type(Candidate.class).orderKey(true).list();

        Random rnd = new Random();
        for(Party party : parties){
            for (int i = 0; i<60; ++i){
                Post post;
                if(rnd.nextInt(100) % 2 == 0) {
                    post = new Post(0, party.getId(), "Party Post " + i, "p1.jpg", "<p>This is content for party post " + i + "<p>", new Date().getTime() - rnd.nextInt(3600 * 1000));
                }else{
                    post = new Post(0, party.getId(), "Party Post " + i, "#", "<p>This is content for party post " + i + "<p>", new Date().getTime() - rnd.nextInt(3600 * 1000));
                }
                posts.add(post);
            }
        }

        for(Candidate candidate : candidates){
            for (int i = 0; i<60; ++i){
                Post post;
                if(rnd.nextInt(100) % 2 == 0) {
                    post = new Post(1, candidate.getId(), "Candidate Post " + i, "p1.jpg", "<p>This is content for candidate post " + i + "<p>", new Date().getTime() - rnd.nextInt(3600 * 1000));
                }else{
                    post = new Post(1, candidate.getId(), "Candidate Post " + i, "#", "<p>This is content for candidate post " + i + "<p>", new Date().getTime() - rnd.nextInt(3600 * 1000));
                }
                posts.add(post);
            }
        }
        ofy().save().entities(posts).now();

        return updateLastUpdate(DATASTORE_POST);
    }

    /**
     * Updates {@link LastUpdate} Table for the given variable
     * @param datastoreLastUpdateVarValue table variable value
     * @return {@code LastUpdate} with final values for the update
     */
    private LastUpdate updateLastUpdate(String datastoreLastUpdateVarValue) {
        LastUpdate lastUpdate  = ofy().load().type(LastUpdate.class)
                .id(datastoreLastUpdateVarValue).now();

        if(lastUpdate == null)
            lastUpdate = new LastUpdate(datastoreLastUpdateVarValue, new Date().getTime());

        //if entry was in Datastore need to update the time
        lastUpdate.setTimestamp(new Date().getTime());

        ofy().save().entity(lastUpdate).now();

        return lastUpdate;
    }

    private Country checkIfExists(Country item){
        Country possibleMatch = ofy().load().type(Country.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private CandidateType checkIfExists(CandidateType item){
        CandidateType possibleMatch = ofy().load().type(CandidateType.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private Candidate checkIfExists(Candidate item){
        Candidate possibleMatch =  ofy().load().type(Candidate.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private Party checkIfExists(Party item){
        Party possibleMatch =  ofy().load().type(Party.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private Post checkIfExists(Post item){
        Post possibleMatch =  ofy().load().type(Post.class).filter("title", item.getTitle()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private State checkIfExists(State item){
        State possibleMatch = ofy().load().type(State.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private District checkIfExists(District item){
        District possibleMatch =  ofy().load().type(District.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }

    private LGA checkIfExists(LGA item){
        LGA possibleMatch = ofy().load().type(LGA.class).filter(DATASTORE_NAME, item.getName()).first().now();
        return possibleMatch == null ? item : possibleMatch;
    }
}
