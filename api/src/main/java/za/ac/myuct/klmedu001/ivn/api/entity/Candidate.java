package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/24.
 * Candidate Model
 */
@Entity
public class Candidate {
    @Id
    @Index
    long Id;
    String name;
    long DOB;
    String POB;
    String currentJob;
    @Index long runningFor;
    @Index long regionId;
    @Index long partyId;
    int termNumber;
    String personalInfo;
    String manifesto;
    String picPath;
    long followers;

    public Candidate() {}

    public Candidate(String name, long DOB, String POB, String currentJob, long runningFor,
                     long regionId, long partyId, int termNumber, String personalInfo, String manifesto,
                     String picPath, long followers) {
        this.name = name;
        this.DOB = DOB;
        this.POB = POB;
        this.currentJob = currentJob;
        this.runningFor = runningFor;
        this.regionId = regionId;
        this.partyId = partyId;
        this.termNumber = termNumber;
        this.personalInfo = personalInfo;
        this.manifesto = manifesto;
        this.picPath = picPath;
        this.followers = followers;
    }

    public Candidate(String [] inputData){
        if(inputData.length != 12)
            throw new IllegalArgumentException("Array Must can exactly 12 elements");

        this.name = inputData[0];
        this.DOB = Long.parseLong(inputData[1]);
        this.POB = inputData[2];
        this.currentJob = inputData[3];
        this.runningFor = Long.parseLong(inputData[4]);
        this.regionId = Long.parseLong(inputData[5]);
        this.partyId = Long.parseLong(inputData[6]);
        this.termNumber = Integer.parseInt(inputData[7]);
        this.personalInfo = inputData[8];
        this.manifesto = inputData[9];
        this.picPath = inputData[10];
        this.followers = Long.parseLong(inputData[11]);

    }

    public long getId() { return Id; }

    public void setId(long id) { Id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public long getDOB() { return DOB; }

    public void setDOB(long DOB) { this.DOB = DOB; }

    public String getPOB() { return POB; }

    public void setPOB(String POB) { this.POB = POB; }

    public String getCurrentJob() { return currentJob; }

    public void setCurrentJob(String currentJob) { this.currentJob = currentJob; }

    public long getRunningFor() { return runningFor; }

    public void setRunningFor(long runningFor) { this.runningFor = runningFor; }

    public long getRegionId() { return regionId; }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public long getPartyId() {
        return partyId;
    }

    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public int getTermNumber() {
        return termNumber;
    }

    public void setTermNumber(int termNumber) {
        this.termNumber = termNumber;
    }

    public String getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(String personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getManifesto() {
        return manifesto;
    }

    public void setManifesto(String manifesto) {
        this.manifesto = manifesto;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }
}
