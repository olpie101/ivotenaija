package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.NotFoundException;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.Party;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Party Endpoint
 */
@Api(
        name = "partyApi",
        version = "v1",
        resource = "party",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class PartyEndpoint {

    private static final Logger logger = Logger.getLogger(PartyEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns list of all {@link za.ac.myuct.klmedu001.ivn.api.entity.Party} entities.
     *
     * @return list of all {@link za.ac.myuct.klmedu001.ivn.api.entity.Party} entities.
     *
     */
    @ApiMethod(
            name = "getAllParties",
            path = "party",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Party> getAllParties() {
        logger.info("Getting all Parties");
        return ofy().load().type(Party.class).list();
    }

    /**
     * Returns the {@link Party} with the corresponding ID.
     *
     * @param id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code Party} with the provided ID.
     */
    @ApiMethod(
            name = "getPartyById",
            path = "party/{id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Party get(@Named("id") long id) throws NotFoundException {
        logger.info("Getting Party with ID: " + id);
        Party party = ofy().load().type(Party.class).id(id).now();
        if (party == null) {
            throw new NotFoundException("Could not find Party with ID: " + id);
        }
        return party;
    }

    /**
     * Inserts a new {@code Party}.
     */
    @ApiMethod(
            name = "insert",
            path = "party",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Party insert(Party party) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that party.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(party).now();
        logger.info("Created Party with ID: " + party.getId());

        return ofy().load().entity(party).now();
    }

    /**
     * Updates an existing {@code Party}.
     *
     * @param Id    the ID of the entity to be updated
     * @param party the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Party}
     */
    @ApiMethod(
            name = "update",
            path = "party/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Party update(@Named("Id") long Id, Party party) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(party).now();
        logger.info("Updated Party: " + party);
        return ofy().load().entity(party).now();
    }

    /**
     * Deletes the specified {@code Party}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Party}
     */
    @ApiMethod(
            name = "remove",
            path = "party/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(Party.class).id(Id).now();
        logger.info("Deleted Party with ID: " + Id);
    }

    @ApiMethod(
            name = "getByCountryId",
            path = "getByCountryId/{countryId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Party> getByCountryId(@Named("countryId") long countryId){
        List<Party> parties = ofy().load().type(Party.class).filter("countryId", countryId).list();
        return parties;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(Party.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Party with ID: " + Id);
        }
    }
}