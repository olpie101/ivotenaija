package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * LGA (Local Government Area) Model
 */
@Entity
public class LGA {
    @Id
    @Index
    Long Id;
    @Index String name;

    @Index long stateId;

    public LGA() {
    }

    public LGA(String name, long stateId) {
        this.name = name;
        this.stateId = stateId;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStateId() {
        return stateId;
    }

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }
}
