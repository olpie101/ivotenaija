package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.Country;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Country Endpoint
 */
@Api(
        name = "countryApi",
        version = "v1",
        resource = "country",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class CountryEndpoint {

    private static final Logger logger = Logger.getLogger(CountryEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns the {@link za.ac.myuct.klmedu001.ivn.api.entity.Country} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code Country} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            //path = "country/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Country get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting Country with ID: " + Id);
        Country country = ofy().load().type(Country.class).id(Id).now();
        if (country == null) {
            throw new NotFoundException("Could not find Country with ID: " + Id);
        }
        return country;
    }

    /**
     * Inserts a new {@code Country}.
     */
    @ApiMethod(
            name = "insert",
            //path = "country",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Country insert(Country country) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that country.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(country).now();
        logger.info("Created Country with ID: " + country.getId());

        return ofy().load().entity(country).now();
    }

    /**
     * Updates an existing {@code Country}.
     *
     * @param Id      the ID of the entity to be updated
     * @param country the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Country}
     */
    @ApiMethod(
            name = "update",
            //path = "country/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Country update(@Named("Id") long Id, Country country) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(country).now();
        logger.info("Updated Country: " + country);
        return ofy().load().entity(country).now();
    }

    /**
     * Deletes the specified {@code Country}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Country}
     */
    @ApiMethod(
            name = "remove",
            //path = "country/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(Country.class).id(Id).now();
        logger.info("Deleted Country with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            //path = "country",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Country> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Country> query = ofy().load().type(Country.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Country> queryIterator = query.iterator();
        List<Country> countryList = new ArrayList<Country>(limit);
        while (queryIterator.hasNext()) {
            countryList.add(queryIterator.next());
        }
        return CollectionResponse.<Country>builder().setItems(countryList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(Country.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Country with ID: " + Id);
        }
    }
}