package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.LastUpdate;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/12/03.
 * Last update endpoint
 */
@Api(
        name = "lastUpdateApi",
        version = "v1",
        resource = "lastUpdate",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class LastUpdateEndpoint {
    private static final Logger logger = Logger.getLogger(LastUpdateEndpoint.class.getName());
    
    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns the {@link za.ac.myuct.klmedu001.ivn.api.entity.LastUpdate} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws com.google.api.server.spi.response.NotFoundException if there is no {@code LastUpdate} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            //path = "lastUpdate/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LastUpdate get(@Named("Id") String Id) throws NotFoundException {
        logger.info("Getting LastUpdate with ID: " + Id);
        LastUpdate lastUpdate = ofy().load().type(LastUpdate.class).id(Id).now();
        if (lastUpdate == null) {
            throw new NotFoundException("Could not find LastUpdate with ID: " + Id);
        }
        return lastUpdate;
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            //path = "lastUpdate",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<LastUpdate> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<LastUpdate> query = ofy().load().type(LastUpdate.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<LastUpdate> queryIterator = query.iterator();
        List<LastUpdate> lastUpdateList = new ArrayList<LastUpdate>(limit);
        while (queryIterator.hasNext()) {
            lastUpdateList.add(queryIterator.next());
        }
        return CollectionResponse.<LastUpdate>builder().setItems(lastUpdateList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }
}
