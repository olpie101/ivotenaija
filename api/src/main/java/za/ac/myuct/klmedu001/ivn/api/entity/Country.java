package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Country Model
 */
@Entity
public class Country {
    @Id
    @Index
    Long Id;
    @Index String name;
    String types;

    public Country() {
    }

    public Country(String name, String types) {
        this.name = name;
        this.types = types;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }
}
