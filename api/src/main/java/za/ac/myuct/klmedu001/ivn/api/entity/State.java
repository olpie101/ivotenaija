package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * State Model
 */
@Entity
public class State {
    @Id
    @Index
    Long Id;
    @Index String name;
    @Index long countryId;

    public State() {
    }

    public State(String name, long countryId) {
        this.name = name;
        this.countryId = countryId;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
}
