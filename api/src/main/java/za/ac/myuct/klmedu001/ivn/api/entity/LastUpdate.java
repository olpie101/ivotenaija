package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Last Update times for a particular entity
 */
@Entity
public class LastUpdate {
    @Id
    @Index
    String name;
    long timestamp;

    public LastUpdate() {
    }

    public LastUpdate(String name, long timestamp) {
        this.name = name;
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
