package za.ac.myuct.klmedu001.ivn.api;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Constants
 */
public interface Constants {
    final String DATASTORE_COUNTRY = "country";
    final String DATASTORE_CANDIDATE_TYPE = "candidate-type";
    final String DATASTORE_CANDIDATE = "candidate";
    final String DATASTORE_NAME = "name";
    final String DATASTORE_PARTY = "party";
    final String DATASTORE_STATE = "state";
    final String DATASTORE_DISTRICT = "district";
    final String DATASTORE_LGA = "lga";
    final String DATASTORE_POST = "post";

    final String FILEPATH_COUNTRY = "assets/countries.csv";
    final String FILEPATH_CANDIDATE_TYPE = "assets/candidate-types.csv";
    final String FILEPATH_CANDIDATE = "assets/candidates.csv";
    final String FILEPATH_PARTY = "assets/parties.csv";
    final String FILEPATH_STATE = "assets/states.csv";
    final String FILEPATH_DISTRICT = "assets/districts.csv";
    final String FILEPATH_LGA = "assets/lgas.csv";
}
