package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.District;
import za.ac.myuct.klmedu001.ivn.api.entity.State;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * District Endpoint
 */
@Api(
        name = "districtApi",
        version = "v1",
        resource = "district",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class DistrictEndpoint {
    private static final Logger logger = Logger.getLogger(DistrictEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns the {@link District} with the corresponding ID.
     *
     * @param id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws com.google.api.server.spi.response.NotFoundException if there is no {@code District} with the provided ID.
     */
    @ApiMethod(
            name = "getDistrictById",
            path = "party/{id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public District get(@Named("id") long id) throws NotFoundException {
        logger.info("Getting District with ID: " + id);
        District party = ofy().load().type(District.class).id(id).now();
        if (party == null) {
            throw new NotFoundException("Could not find District with ID: " + id);
        }
        return party;
    }

    /**
     * Inserts a new {@code District}.
     */
    @ApiMethod(
            name = "insert",
            path = "party",
            httpMethod = ApiMethod.HttpMethod.POST)
    public District insert(District party) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that party.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(party).now();
        logger.info("Created District with ID: " + party.getId());

        return ofy().load().entity(party).now();
    }

    /**
     * Updates an existing {@code District}.
     *
     * @param Id    the ID of the entity to be updated
     * @param party the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code District}
     */
    @ApiMethod(
            name = "update",
            path = "party/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public District update(@Named("Id") long Id, District party) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(party).now();
        logger.info("Updated District: " + party);
        return ofy().load().entity(party).now();
    }

    /**
     * Deletes the specified {@code District}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code District}
     */
    @ApiMethod(
            name = "remove",
            path = "party/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(District.class).id(Id).now();
        logger.info("Deleted District with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "candidate",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<District> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<District> query = ofy().load().type(District.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<District> queryIterator = query.iterator();
        List<District> candidateList = new ArrayList<District>(limit);
        while (queryIterator.hasNext()) {
            candidateList.add(queryIterator.next());
        }
        return CollectionResponse.<District>builder().setItems(candidateList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    @ApiMethod(
            name = "getByCountryId",
            path = "getByCountryId/{countryId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<District> getByCountryId(@Named("countryId") long countryId){
        List<State> stateList = ofy().load().type(State.class).filter("countryId", countryId).list();
        List<District> districtList = new ArrayList<>();

        for (State state : stateList) {
            districtList.addAll(ofy().load().type(District.class).filter("stateId", state.getId()).list());
        }
        return districtList;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(District.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find District with ID: " + Id);
        }
    }
}
