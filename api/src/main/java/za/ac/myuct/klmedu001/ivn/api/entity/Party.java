package za.ac.myuct.klmedu001.ivn.api.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Political Party Model
 */
@Entity
public class Party {
    @Id
    @Index
    Long Id;
    String name;
    String motto;
    String logoPath;
    String info;

    @Index
    long countryId;

    public Party() {
    }

    public Party(String name, String motto, String logoPath, String info, long countryId) {
        this.name = name;
        this.motto = motto;
        this.logoPath = logoPath;
        this.info = info;
        this.countryId = countryId;
    }

    public Party (String [] inputData){
        if(inputData.length != 5)
            throw new IllegalArgumentException("input array must have exactly 5 elements");

        this.name = inputData[0];
        this.motto = inputData[1];
        this.logoPath = inputData[2];
        this.info = inputData[3];
        this.countryId = Long.parseLong(inputData[4]);
    }

    public long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
}
