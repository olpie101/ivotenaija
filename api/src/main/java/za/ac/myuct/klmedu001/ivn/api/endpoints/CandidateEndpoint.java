package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.Candidate;
import za.ac.myuct.klmedu001.ivn.api.entity.Party;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Candidate Endpoint
 */
@Api(
        name = "candidateApi",
        version = "v1",
        resource = "candidate",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class CandidateEndpoint {

    private static final Logger logger = Logger.getLogger(CandidateEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(Candidate.class);
    }

    /**
     * Returns the {@link Candidate} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code Candidate} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "candidate/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Candidate get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting Candidate with ID: " + Id);
        Candidate candidate = ofy().load().type(Candidate.class).id(Id).now();
        if (candidate == null) {
            throw new NotFoundException("Could not find Candidate with ID: " + Id);
        }
        return candidate;
    }

    /**
     * Inserts a new {@code Candidate}.
     */
    @ApiMethod(
            name = "insert",
            path = "candidate",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Candidate insert(Candidate candidate) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that candidate.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(candidate).now();
        logger.info("Created Candidate with ID: " + candidate.getId());

        return ofy().load().entity(candidate).now();
    }

    /**
     * Updates an existing {@code Candidate}.
     *
     * @param Id        the ID of the entity to be updated
     * @param candidate the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Candidate}
     */
    @ApiMethod(
            name = "update",
            path = "candidate/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Candidate update(@Named("Id") long Id, Candidate candidate) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(candidate).now();
        logger.info("Updated Candidate: " + candidate);
        return ofy().load().entity(candidate).now();
    }

    /**
     * Deletes the specified {@code Candidate}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Candidate}
     */
    @ApiMethod(
            name = "remove",
            path = "candidate/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(Candidate.class).id(Id).now();
        logger.info("Deleted Candidate with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "candidate",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Candidate> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Candidate> query = ofy().load().type(Candidate.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Candidate> queryIterator = query.iterator();
        List<Candidate> candidateList = new ArrayList<Candidate>(limit);
        while (queryIterator.hasNext()) {
            candidateList.add(queryIterator.next());
        }
        return CollectionResponse.<Candidate>builder().setItems(candidateList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    /**
     * Return list of all {@link Candidate}s that belong to corresponding
     * {@link za.ac.myuct.klmedu001.ivn.api.entity.Party} with Id
     * @param partyId Id of party to query
     * @return list of all {@link Candidate}s that belong to corresponding
     * {@link za.ac.myuct.klmedu001.ivn.api.entity.Party}
     */
    @ApiMethod(
            name = "getByPartyId",
            path = "candidateByPartyId/{partyId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Candidate> getByPartyId(@Named("partyId") long partyId){
        return ofy().load().type(Candidate.class).filter("partyId", partyId).orderKey(false).list();
    }

    /**
     * Return list of all {@link Candidate}s that belong to corresponding
     * {@link za.ac.myuct.klmedu001.ivn.api.entity.Country} with Id
     * @param countryId Id of country to query
     * @return list of all {@link Candidate}s that belong to corresponding
     * {@link za.ac.myuct.klmedu001.ivn.api.entity.Country}
     */
    @ApiMethod(
            name = "getByCountryId",
            path = "candidateByCountryId/{countryId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Candidate> getByCountryId(@Named("countryId") long countryId) {
        List<Party> partiesForCountry = ofy().load().type(Party.class).filter("countryId", countryId).list();
        List<Candidate> candidateList = new ArrayList<>();

        for (Party party : partiesForCountry) {
            candidateList.addAll(ofy().load().type(Candidate.class).filter("partyId", party.getId()).list());
        }
        return candidateList;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(Candidate.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Candidate with ID: " + Id);
        }
    }
}