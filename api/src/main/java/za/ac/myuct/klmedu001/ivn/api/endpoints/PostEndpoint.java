package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.Post;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Candidate Type Endpoint
 */
@Api(
        name = "postApi",
        version = "v1",
        resource = "post",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class PostEndpoint {

    private static final Logger logger = Logger.getLogger(PostEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns the {@link Post} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code Post} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "post/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public Post get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting Post with ID: " + Id);
        Post post = ofy().load().type(Post.class).id(Id).now();
        if (post == null) {
            throw new NotFoundException("Could not find Post with ID: " + Id);
        }
        return post;
    }

    /**
     * Inserts a new {@code Post}.
     */
    @ApiMethod(
            name = "insert",
            path = "post",
            httpMethod = ApiMethod.HttpMethod.POST)
    public Post insert(Post post) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that post.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(post).now();
        logger.info("Created Post with ID: " + post.getId());

        return ofy().load().entity(post).now();
    }

    /**
     * Updates an existing {@code Post}.
     *
     * @param Id   the ID of the entity to be updated
     * @param post the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Post}
     */
    @ApiMethod(
            name = "update",
            path = "post/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public Post update(@Named("Id") long Id, Post post) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(post).now();
        logger.info("Updated Post: " + post);
        return ofy().load().entity(post).now();
    }

    /**
     * Deletes the specified {@code Post}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code Post}
     */
    @ApiMethod(
            name = "remove",
            path = "post/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(Post.class).id(Id).now();
        logger.info("Deleted Post with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "post",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<Post> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<Post> query = ofy().load().type(Post.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<Post> queryIterator = query.iterator();
        List<Post> postList = new ArrayList<Post>(limit);
        while (queryIterator.hasNext()) {
            postList.add(queryIterator.next());
        }
        return CollectionResponse.<Post>builder().setItems(postList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    /**
     * Return all {@link Post}s from user with corresponding userId and userType
     *
     * @param userType type of given user
     * @param userId   Id of given user
     * @return list of {link Post} entities from user
     *
     * @throws NotFoundException if user with corresponding userType and userId is not found
     */
    @ApiMethod(
            name = "getPostsByUser",
            path = "postsbyuser",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Post> getPostsByUser(@Named("userType") long userType, @Named("userId") long userId) throws NotFoundException {
        logger.info("Getting Posts with userType = " + userType + " & userId = " + userId);
        List<Post> posts = ofy().load().type(Post.class).filter("userType", userType)
                .filter("userId", userId).order("-timestamp").list();
        if (posts == null || posts.size() == 0)
            throw new NotFoundException("Could not find any posts from userId = " + userId + "with userType = " + userType);
        else
            return posts;
    }

    /**
     * Return all {@link Post}s from user with corresponding userId and userType
     *
     * @param userType type of given user
     * @param userId   Id of given user
     * @return list of {link Post} entities from user
     *
     * @throws NotFoundException if user with corresponding userType and userId is not found
     */
    @ApiMethod(
            name = "getPostsByUserAfterTimestamp",
            path = "postsbyuseraftertimestamp",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<Post> getPostsByUserAfterTimestamp(
            @Named("userType") long userType, @Named("userId") long userId, @Named("fromTimestamp") long fromTimestamp) throws NotFoundException {
        logger.info("Getting Posts with userType = " + userType + " & userId = " + userId+ " & timestamp = "+fromTimestamp);
        List<Post> posts = ofy().load().type(Post.class).filter("userType", userType)
                .filter("userId", userId).filter("timestamp >", fromTimestamp).order("-timestamp").list();
        if (posts == null || posts.size() == 0)
            throw new NotFoundException("Could not find any posts from userId = " + userId + "with userType = " + userType);
        else
            return posts;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(Post.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find Post with ID: " + Id);
        }
    }
}