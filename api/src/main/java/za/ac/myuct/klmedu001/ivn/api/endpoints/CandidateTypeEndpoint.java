package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.OfyService;
import za.ac.myuct.klmedu001.ivn.api.entity.CandidateType;
import za.ac.myuct.klmedu001.ivn.api.entity.Country;

import static com.googlecode.objectify.ObjectifyService.ofy;


/**
 * Created by eduardokolomajr on 2014/11/25.
 * CandidateType Endpoint
 */
@Api(
        name = "candidateTypeApi",
        version = "v1",
        resource = "candidateType",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class CandidateTypeEndpoint {

    private static final Logger logger = Logger.getLogger(CandidateTypeEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(CandidateType.class);
    }

    /**
     * Returns the {@link CandidateType} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code CandidateType} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "candidateType/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CandidateType get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting CandidateType with ID: " + Id);
        CandidateType candidateType = ofy().load().type(CandidateType.class).id(Id).now();
        if (candidateType == null) {
            throw new NotFoundException("Could not find CandidateType with ID: " + Id);
        }
        return candidateType;
    }

    /**
     * Inserts a new {@code CandidateType}.
     */
    @ApiMethod(
            name = "insert",
            path = "candidateType",
            httpMethod = ApiMethod.HttpMethod.POST)
    public CandidateType insert(CandidateType candidateType) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that candidateType.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(candidateType).now();
        logger.info("Created CandidateType with ID: " + candidateType.getId());

        return ofy().load().entity(candidateType).now();
    }

    /**
     * Updates an existing {@code CandidateType}.
     *
     * @param Id            the ID of the entity to be updated
     * @param candidateType the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code CandidateType}
     */
    @ApiMethod(
            name = "update",
            path = "candidateType/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public CandidateType update(@Named("Id") long Id, CandidateType candidateType) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(candidateType).now();
        logger.info("Updated CandidateType: " + candidateType);
        return ofy().load().entity(candidateType).now();
    }

    /**
     * Deletes the specified {@code CandidateType}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code CandidateType}
     */
    @ApiMethod(
            name = "remove",
            path = "candidateType/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(CandidateType.class).id(Id).now();
        logger.info("Deleted CandidateType with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "candidateType",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<CandidateType> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<CandidateType> query = ofy().load().type(CandidateType.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<CandidateType> queryIterator = query.iterator();
        List<CandidateType> candidateTypeList = new ArrayList<CandidateType>(limit);
        while (queryIterator.hasNext()) {
            candidateTypeList.add(queryIterator.next());
        }
        return CollectionResponse.<CandidateType>builder().setItems(candidateTypeList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    /**
     * Gets list of {@link CandidateType} entities based on the country which pertains to the
     * Id provided.
     * @param countryId Id of country for wanted CandidateTypes
     * @return Candidate Types for particular country
     * @throws NotFoundException If country with <code>countryId</code> Id is not found
     */
    @ApiMethod(name = "getCandidateTypesByCountryId")
    public List<CandidateType> getCandidateTypeByCountryId(@Named("countryId") long countryId) throws NotFoundException{
        logger.info("Getting Candidate Type with Country Id = "+countryId);
        Country c = OfyService.ofy().load().type(Country.class).id(countryId).now();

        if(c == null)
            throw new NotFoundException("Could not find country with Id = "+countryId);

        String [] typesString = c.getTypes().split("-");

        List<CandidateType> types = new ArrayList<>(5);

        for(String typeString : typesString){
            CandidateType ct = OfyService.ofy().load().type(CandidateType.class).id(Integer.parseInt(typeString)).now();

            if (ct == null)
                logger.info("Could not find Candidate Type with Id = "+typeString);
            else
                types.add(ct);
        }

        return types;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(CandidateType.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find CandidateType with ID: " + Id);
        }
    }
}