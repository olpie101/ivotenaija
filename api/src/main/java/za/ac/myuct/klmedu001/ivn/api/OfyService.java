package za.ac.myuct.klmedu001.ivn.api;

/**
 * Created by eduardokolomajr on 2014/09/16.
 */
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import za.ac.myuct.klmedu001.ivn.api.entity.Candidate;
import za.ac.myuct.klmedu001.ivn.api.entity.CandidateType;
import za.ac.myuct.klmedu001.ivn.api.entity.Comment;
import za.ac.myuct.klmedu001.ivn.api.entity.Country;
import za.ac.myuct.klmedu001.ivn.api.entity.District;
import za.ac.myuct.klmedu001.ivn.api.entity.Event;
import za.ac.myuct.klmedu001.ivn.api.entity.LGA;
import za.ac.myuct.klmedu001.ivn.api.entity.LastUpdate;
import za.ac.myuct.klmedu001.ivn.api.entity.Party;
import za.ac.myuct.klmedu001.ivn.api.entity.Post;
import za.ac.myuct.klmedu001.ivn.api.entity.State;


/**
 * Objectify service wrapper so we can statically register our persistence classes
 * More on Objectify here : https://code.google.com/p/objectify-appengine/
 *
 */
public class OfyService {

    static {
        ObjectifyService.register(Candidate.class);
        ObjectifyService.register(CandidateType.class);
        ObjectifyService.register(Comment.class);
        ObjectifyService.register(Country.class);
        ObjectifyService.register(District.class);
        ObjectifyService.register(Event.class);
        ObjectifyService.register(LGA.class);
        ObjectifyService.register(Party.class);
        ObjectifyService.register(Post.class);
        ObjectifyService.register(State.class);
        ObjectifyService.register(LGA.class);
        ObjectifyService.register(LastUpdate.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
