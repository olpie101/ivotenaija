package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.NotFoundException;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.State;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;

/**
 * Created by eduardokolomajr on 2014/11/25.
 * Candidate Type Endpoint
 */
@Api(
        name = "stateApi",
        version = "v1",
        resource = "state",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class StateEndpoint {

    private static final Logger logger = Logger.getLogger(StateEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    /**
     * Returns list {@link State} entities available
     *
     * @return list {@link State} entities available
     */
    @ApiMethod(
            name = "getAllStates",
            path = "state/",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<State> getAllStates() throws NotFoundException {
        logger.info("Getting all States: ");
        return ofy().load().type(State.class).orderKey(false).list();
    }

    /**
     * Returns the {@link State} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code State} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "state/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public State get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting State with ID: " + Id);
        State state = ofy().load().type(State.class).id(Id).now();
        if (state == null) {
            throw new NotFoundException("Could not find State with ID: " + Id);
        }
        return state;
    }

    /**
     * Returns list of all {@link State} entities with the corresponding
     * {@link za.ac.myuct.klmedu001.ivn.api.entity.Country}ID.
     *
     * @param countryId the ID of the {@link za.ac.myuct.klmedu001.ivn.api.entity.Country} of the entities to be retrieved
     * @return list of entities with the corresponding {@link za.ac.myuct.klmedu001.ivn.api.entity.Country}
     *
     * @throws NotFoundException if there is no {@code Country} with the provided ID.
     */
    @ApiMethod(
            name = "getStatesByCountryId",
            path = "statesByCountryId/{countryId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<State> getStatesById(@Named("countryId") long countryId) throws NotFoundException {
        logger.info("Getting State with country with ID ID: " + countryId);
        List<State> states = ofy().load().type(State.class).filter("countryId", countryId).list();
        if (states == null || states.size() == 0) {
            throw new NotFoundException("Could not find States with country ID: " + countryId);
        }else {
            return states;
        }
    }

    /**
     * Inserts a new {@code State}.
     */
    @ApiMethod(
            name = "insert",
            path = "state",
            httpMethod = ApiMethod.HttpMethod.POST)
    public State insert(State state) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that state.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(state).now();
        logger.info("Created State with ID: " + state.getId());

        return ofy().load().entity(state).now();
    }

    /**
     * Updates an existing {@code State}.
     *
     * @param Id    the ID of the entity to be updated
     * @param state the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code State}
     */
    @ApiMethod(
            name = "update",
            path = "state/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public State update(@Named("Id") long Id, State state) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(state).now();
        logger.info("Updated State: " + state);
        return ofy().load().entity(state).now();
    }

    /**
     * Deletes the specified {@code State}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code State}
     */
    @ApiMethod(
            name = "remove",
            path = "state/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(State.class).id(Id).now();
        logger.info("Deleted State with ID: " + Id);
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(State.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find State with ID: " + Id);
        }
    }
}