package za.ac.myuct.klmedu001.ivn.api.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.inject.Named;

import za.ac.myuct.klmedu001.ivn.api.entity.LGA;
import za.ac.myuct.klmedu001.ivn.api.entity.State;

import static za.ac.myuct.klmedu001.ivn.api.OfyService.ofy;


/**
 * Created by eduardokolomajr on 2014/11/25.
 * LGA Endpoint
 */
@Api(
        name = "lGAApi",
        version = "v1",
        resource = "lGA",
        namespace = @ApiNamespace(
                ownerDomain = "entity.api.ivn.klmedu001.myuct.ac.za",
                ownerName = "entity.api.ivn.klmedu001.myuct.ac.za",
                packagePath = ""
        )
)
public class LGAEndpoint {

    private static final Logger logger = Logger.getLogger(LGAEndpoint.class.getName());

    private static final int DEFAULT_LIST_LIMIT = 20;

    static {
        // Typically you would register this inside an OfyServive wrapper. See: https://code.google.com/p/objectify-appengine/wiki/BestPractices
        ObjectifyService.register(LGA.class);
    }

    /**
     * Returns the {@link LGA} with the corresponding ID.
     *
     * @param Id the ID of the entity to be retrieved
     * @return the entity with the corresponding ID
     *
     * @throws NotFoundException if there is no {@code LGA} with the provided ID.
     */
    @ApiMethod(
            name = "get",
            path = "lGA/{Id}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public LGA get(@Named("Id") long Id) throws NotFoundException {
        logger.info("Getting LGA with ID: " + Id);
        LGA lGA = ofy().load().type(LGA.class).id(Id).now();
        if (lGA == null) {
            throw new NotFoundException("Could not find LGA with ID: " + Id);
        }
        return lGA;
    }

    /**
     * Inserts a new {@code LGA}.
     */
    @ApiMethod(
            name = "insert",
            path = "lGA",
            httpMethod = ApiMethod.HttpMethod.POST)
    public LGA insert(LGA lGA) {
        // Typically in a RESTful API a POST does not have a known ID (assuming the ID is used in the resource path).
        // You should validate that lGA.Id has not been set. If the ID type is not supported by the
        // Objectify ID generator, e.g. long or String, then you should generate the unique ID yourself prior to saving.
        //
        // If your client provides the ID then you should probably use PUT instead.
        ofy().save().entity(lGA).now();
        logger.info("Created LGA with ID: " + lGA.getId());

        return ofy().load().entity(lGA).now();
    }

    /**
     * Updates an existing {@code LGA}.
     *
     * @param Id  the ID of the entity to be updated
     * @param lGA the desired state of the entity
     * @return the updated version of the entity
     *
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code LGA}
     */
    @ApiMethod(
            name = "update",
            path = "lGA/{Id}",
            httpMethod = ApiMethod.HttpMethod.PUT)
    public LGA update(@Named("Id") long Id, LGA lGA) throws NotFoundException {
        // TODO: You should validate your ID parameter against your resource's ID here.
        checkExists(Id);
        ofy().save().entity(lGA).now();
        logger.info("Updated LGA: " + lGA);
        return ofy().load().entity(lGA).now();
    }

    /**
     * Deletes the specified {@code LGA}.
     *
     * @param Id the ID of the entity to delete
     * @throws NotFoundException if the {@code Id} does not correspond to an existing
     *                           {@code LGA}
     */
    @ApiMethod(
            name = "remove",
            path = "lGA/{Id}",
            httpMethod = ApiMethod.HttpMethod.DELETE)
    public void remove(@Named("Id") long Id) throws NotFoundException {
        checkExists(Id);
        ofy().delete().type(LGA.class).id(Id).now();
        logger.info("Deleted LGA with ID: " + Id);
    }

    /**
     * List all entities.
     *
     * @param cursor used for pagination to determine which page to return
     * @param limit  the maximum number of entries to return
     * @return a response that encapsulates the result list and the next page token/cursor
     */
    @ApiMethod(
            name = "list",
            path = "lGA",
            httpMethod = ApiMethod.HttpMethod.GET)
    public CollectionResponse<LGA> list(@Nullable @Named("cursor") String cursor, @Nullable @Named("limit") Integer limit) {
        limit = limit == null ? DEFAULT_LIST_LIMIT : limit;
        Query<LGA> query = ofy().load().type(LGA.class).limit(limit);
        if (cursor != null) {
            query = query.startAt(Cursor.fromWebSafeString(cursor));
        }
        QueryResultIterator<LGA> queryIterator = query.iterator();
        List<LGA> lGAList = new ArrayList<LGA>(limit);
        while (queryIterator.hasNext()) {
            lGAList.add(queryIterator.next());
        }
        return CollectionResponse.<LGA>builder().setItems(lGAList).setNextPageToken(queryIterator.getCursor().toWebSafeString()).build();
    }

    @ApiMethod(
            name = "getByCountryId",
            path = "getByCountryId/{countryId}",
            httpMethod = ApiMethod.HttpMethod.GET)
    public List<LGA> getByCountryId(@Named("countryId") long countryId){
        List<State> stateList = ofy().load().type(State.class).filter("countryId", countryId).list();
        List<LGA> lgaList = new ArrayList<>();

        for (State state : stateList) {
            lgaList.addAll(ofy().load().type(LGA.class).filter("stateId", state.getId()).list());
        }
        return lgaList;
    }

    private void checkExists(long Id) throws NotFoundException {
        try {
            ofy().load().type(LGA.class).id(Id).safe();
        } catch (com.googlecode.objectify.NotFoundException e) {
            throw new NotFoundException("Could not find LGA with ID: " + Id);
        }
    }
}