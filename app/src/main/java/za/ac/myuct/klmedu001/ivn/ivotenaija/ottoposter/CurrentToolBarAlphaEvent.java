package za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter;

/**
 * Created by eduardokolomajr on 2015/01/07.
 */
public class CurrentToolBarAlphaEvent {
    public final int alpha;

    public CurrentToolBarAlphaEvent(int alpha) {
        this.alpha = alpha;
    }
}
