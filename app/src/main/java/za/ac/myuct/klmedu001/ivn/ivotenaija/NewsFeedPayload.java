package za.ac.myuct.klmedu001.ivn.ivotenaija;

import java.util.List;

import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PostContainer;

/**
 * Created by eduardokolomajr on 2015/01/15.
 * Payload for newsfeed posts
 */
public class NewsFeedPayload {
    public final boolean top; //true to attach to top false to add to bottom of feed
    public final List<PostContainer> posts;

    public NewsFeedPayload(boolean top, List<PostContainer> posts) {
        this.top = top;
        this.posts = posts;
    }
}
