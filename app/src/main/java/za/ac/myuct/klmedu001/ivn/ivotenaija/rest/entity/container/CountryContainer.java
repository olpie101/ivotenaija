package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.countryApi.model.Country;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for Country
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_COUNTRY)
public class CountryContainer extends BaseModel{
    @Column(columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column
    String name;
    @Column
    String types;

    public CountryContainer() {
    }

    public CountryContainer(Long id, String name, String types) {
        _id = id;
        this.name = name;
        this.types = types;
    }

    public CountryContainer(Country country){
        this._id = country.getId();
        this.name = country.getName();
        this.types = country.getTypes();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public Country getCountry(){
        Country country = new Country();
        country.setId(_id);
        country.setName(name);
        country.setTypes(types);
        return country;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.countryApi.model.Country} to a
     * list of {@code CountryContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code Country} which is to be converted
     * @return converted list
     */
    public static List<CountryContainer> fromCountryList(List<Country> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<CountryContainer> containerList = new ArrayList<>(list.size());

        for (Country country : list) {
            containerList.add(new CountryContainer(country));
        }
        return containerList;
    }
}

