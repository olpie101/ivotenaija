package za.ac.myuct.klmedu001.ivn.ivotenaija.constant;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.HttpTransport;

import za.ac.myuct.klmedu001.ivn.api.entity.candidateApi.CandidateApi;
import za.ac.myuct.klmedu001.ivn.api.entity.candidateTypeApi.CandidateTypeApi;
import za.ac.myuct.klmedu001.ivn.api.entity.countryApi.CountryApi;
import za.ac.myuct.klmedu001.ivn.api.entity.districtApi.DistrictApi;
import za.ac.myuct.klmedu001.ivn.api.entity.lGAApi.LGAApi;
import za.ac.myuct.klmedu001.ivn.api.entity.lastUpdateApi.LastUpdateApi;
import za.ac.myuct.klmedu001.ivn.api.entity.partyApi.PartyApi;
import za.ac.myuct.klmedu001.ivn.api.entity.postApi.PostApi;
import za.ac.myuct.klmedu001.ivn.api.entity.stateApi.StateApi;

/**
 * Created by eduardokolomajr on 2014/11/16.
 * IVN Constants
 */
public class IVNConstants {
//    public static final String API_URL = "http://www.wcidp.co.za/Ivnservice/odata";
    public static final String AE_NAME = "steam-cache-781";
//    public static final String AE_URL = "http://10.0.3.2:8080/";
//    public static final String AE_URL = "http://10.0.2.2:8080/";
    public static final String AE_URL = "http://192.168.0.104:8080/";
//    public static final String AE_URL = "https://steam-cache-781.appspot.com/";
    public static final String AE_API_URL = AE_URL+"_ah/api/";
    private static final AndroidJsonFactory androidJsonFactory = new AndroidJsonFactory();
    public static final HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
//    public static final String AE_APP_URL = "app-name-here";

    public static final String PREFS_LAST_UPDATE = "za.za.ac.myuct.klmedu001.ivn.ivotenaija.lastUpdate";
    public static final String LAST_UPDATE_COUNTRY = "country";
    public static final String LAST_UPDATE_CANDIDATE_TYPE = "candidate-type";
    public static final String LAST_UPDATE_CANDIDATE = "candidate";
    public static final String LAST_UPDATE_PARTY = "party";
    public static final String LAST_UPDATE_STATE = "state";
    public static final String LAST_UPDATE_DISTRICT = "district";
    public static final String LAST_UPDATE_LGA = "lga";

    public static final int ASYNCTASKLOADER_NEWSLOADER_ID = 9801;

    public static final int FEED_PARTIES_ID = 0;
    public static final int FEED_CANDIDATES_ID = 1;


    public static final long COUNTRY_ID = 1;

    public static final int DY_SCROLL_THRESHOLD = 50;

    public static final LastUpdateApi.Builder lastUpdateEndpoint =
            new LastUpdateApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final CountryApi.Builder countryEndpoint =
            new CountryApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final CandidateTypeApi.Builder candidateTypeEndpoint =
            new CandidateTypeApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final CandidateApi.Builder candidateEndpoint =
            new CandidateApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final PartyApi.Builder partyEndpoint =
            new PartyApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final StateApi.Builder stateEndpoint =
            new StateApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);
    
    public static final DistrictApi.Builder districtEndpoint = 
            new DistrictApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final LGAApi.Builder LGAEndpoint =
            new LGAApi.Builder(httpTransport, androidJsonFactory, null)
            .setRootUrl(AE_API_URL)
            .setApplicationName(AE_NAME);

    public static final PostApi.Builder postEndpoint =
            new PostApi.Builder(httpTransport, androidJsonFactory, null)
                    .setRootUrl(AE_API_URL)
                    .setApplicationName(AE_NAME);
}
