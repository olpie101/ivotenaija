package za.ac.myuct.klmedu001.ivn.ivotenaija.constant;

import com.grosner.dbflow.annotation.Database;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Database class used by DBFlow
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION, foreignKeysSupported = true)
public class AppDatabase {
    public static final String NAME = "ivn";
    public static final int  VERSION = 1;

    public static final String TABLE_CANDIDATE = "candidate";
    public static final String TABLE_CANDIDATE_TYPE = "candidateType";
    public static final String TABLE_COUNTRY = "country";
    public static final String TABLE_DISTRICT = "district";
    public static final String TABLE_LGA = "lga";
    public static final String TABLE_PARTY = "party";
    public static final String TABLE_STATE = "state";
    public static final String TABLE_FOLLOWING = "entitiesFollowing";
    public static final String TABLE_POST = "post";
}
