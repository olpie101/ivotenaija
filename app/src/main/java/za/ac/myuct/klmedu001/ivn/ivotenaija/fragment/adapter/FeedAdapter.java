package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.grosner.dbflow.sql.builder.Condition;
import com.grosner.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PostContainer;

/**
 * Created by eduardokolomajr on 2014/12/04.
 *
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {
    private static final String TAG = "FeedAdapter";
    private static final int NOT_FOLLOWING_CANDIDATES = -2;
    private static final int NOT_FOLLOWING_PARTIES = -1;
    public static final int FOLLOWING_PARTIES = IVNConstants.FEED_PARTIES_ID;
    public static final int FOLLOWING_CANDIDATES = IVNConstants.FEED_CANDIDATES_ID;

    private int sectionId;
    private List<EntitiesFollowing> followingList;
    private HashMap<Long, CandidateContainer> candidatesList;
    private HashMap<Long, PartyContainer> partiesList;
    private List<PostContainer> posts;

    public FeedAdapter(int sectionId) {
        followingList = new ArrayList<>();
        candidatesList = new HashMap<>();
        partiesList = new HashMap<>();
        posts = new ArrayList<>();

        Log.d(TAG, "constructor start sid="+sectionId);
        Log.d(TAG, "constructor start fl="+followingList.size());
        switch(sectionId){
            case FOLLOWING_PARTIES:
                this.sectionId = FOLLOWING_PARTIES;
                followingList.addAll(new Select().from(EntitiesFollowing.class)
                        .where(Condition.column(EntitiesFollowing$Table.TYPE)
                                .is(EntitiesFollowing.PARTIES)).queryList());
                if(followingList.size() < 1)
                    this.sectionId = NOT_FOLLOWING_PARTIES;
                break;
            case FOLLOWING_CANDIDATES:
                this.sectionId = FOLLOWING_CANDIDATES;
                followingList.addAll(new Select().from(EntitiesFollowing.class)
                        .where(Condition.column(EntitiesFollowing$Table.TYPE)
                                .is(EntitiesFollowing.CANDIDATES)).queryList());
                if(followingList.size() < 1)
                    this.sectionId = NOT_FOLLOWING_CANDIDATES;
                break;
            default:
                break;
        }
        for(EntitiesFollowing entity: followingList){
                if(entity.getType() == IVNConstants.FEED_PARTIES_ID) {
                    PartyContainer currentParty = new Select().from(PartyContainer.class)
                            .where(Condition.column(PartyContainer$Table._ID).is(entity.getId())).querySingle();
                    partiesList.put(currentParty.getId(), currentParty);
                }else{
                    CandidateContainer currentCandidate = new Select().from(CandidateContainer.class)
                            .where(Condition.column(CandidateContainer$Table._ID).is(entity.getId())).querySingle();
                    candidatesList.put(currentCandidate.getId(), currentCandidate);
                }
        }
        Log.d(TAG, "constructor end sid="+this.sectionId);
    }

    public void setPosts(Context ctx, boolean top, List<PostContainer> newPosts){
        Toast.makeText(ctx, sectionId+" Received posts. size = " + newPosts.size(), Toast.LENGTH_SHORT).show();

        posts.addAll(newPosts);
    }

    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (sectionId){
            case 0:
            case 1:
                Log.d(TAG, "creating pager");
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.newsfeed_post_entry, parent, false);
                break;
            default:
                Log.d(TAG, "creating empty");
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.empty_list_item, parent, false);
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedAdapter.ViewHolder holder, int position) {
        switch (sectionId){
            case NOT_FOLLOWING_CANDIDATES:
                holder.emptyListText.setText("You are following 0 candidates.\nAdd candidate to follow");
                break;
            case NOT_FOLLOWING_PARTIES:
                holder.emptyListText.setText("You are following 0 parties.\nAdd a party to follow");
                break;
            case FOLLOWING_PARTIES:
                holder.entryTitle.setText(posts.get(position).getTitle());
                //TODO ensure that replace all html tags in future
                holder.entryContent.setText(posts.get(position).getContent().replace("<p>", "").replace("</p>", ""));
                holder.entryUser.setText(partiesList.get(posts.get(position).getUserId()).getName());

                //If no picture, remove ImageView else make sure it's visible
                if(posts.get(position).getPicPath().equals("#"))
                    holder.entryImage.setVisibility(View.GONE);
                else
                    holder.entryImage.setVisibility(View.VISIBLE);
                break;
            case FOLLOWING_CANDIDATES:
                break;
        }
    }

    @Override
    public int getItemCount() {
        switch (sectionId){
            case FOLLOWING_PARTIES:
            case FOLLOWING_CANDIDATES:
                return posts.size();
            default:
                return 1;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @Optional @InjectView(R.id.tv_feed_empty_text)
        TextView emptyListText;
        @Optional @InjectView(R.id.iv_feed_entry_image)
        ImageView entryImage;
        @Optional @InjectView(R.id.tv_feed_entry_title)
        TextView entryTitle;
        @Optional @InjectView(R.id.tv_feed_entry_content)
        TextView entryContent;
        @Optional @InjectView(R.id.tv_feed_entry_user)
        TextView entryUser;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
