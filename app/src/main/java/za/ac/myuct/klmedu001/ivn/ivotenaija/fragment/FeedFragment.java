package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.InjectView;
import za.ac.myuct.klmedu001.ivn.ivotenaija.NewsFeedPayload;
import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter.FeedAdapter;
import za.ac.myuct.klmedu001.ivn.ivotenaija.loader.NewsFeedLoader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedFragment extends Fragment implements LoaderManager.LoaderCallbacks<NewsFeedPayload>{
    private static final String TAG = "FeedFragment";
    public static final int PARTIES = IVNConstants.FEED_PARTIES_ID;
    public static final int CANDIDATES = IVNConstants.FEED_CANDIDATES_ID;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "feedSectionNumber";

    // TODO: Rename and change types of parameters
    private int sectionNumber;
    private boolean isLoading;

    @InjectView(R.id.rv_feed)
    RecyclerView feedRecyclerView;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sectionNumber section number of fragment on feed.
     * @return A new instance of fragment FeedFragement.
     */
    // TODO: Rename and change types and number of parameters
    public static FeedFragment newInstance(int sectionNumber) {
        Log.d(TAG, "creating instance");
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sectionNumber = getArguments().getInt(ARG_PARAM1);
        }

        getLoaderManager().initLoader(IVNConstants.ASYNCTASKLOADER_NEWSLOADER_ID, null, this);
        isLoading = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.inject(this, v);
        feedRecyclerView.setAdapter(new FeedAdapter(sectionNumber));
        final LinearLayoutManager layoutManager = new LinearLayoutManager(container.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSmoothScrollbarEnabled(true);
        feedRecyclerView.setLayoutManager(layoutManager);
        feedRecyclerView.setVerticalScrollBarEnabled(true);
        feedRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                Log.d(TAG, ""+(float)layoutManager.findFirstVisibleItemPosition()/(float)layoutManager.getItemCount()+" loading = "+isLoading);
                if((float)layoutManager.findFirstVisibleItemPosition()/(float)layoutManager.getItemCount() > 0.6f
                        && !isLoading) {
                    Log.d(TAG, "should load more articles");
                    isLoading = true;
                    Loader<NewsFeedPayload> newsFeedLoader = getLoaderManager().getLoader(IVNConstants.ASYNCTASKLOADER_NEWSLOADER_ID);
                    newsFeedLoader.reset();
                    newsFeedLoader.startLoading();
                }
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    @Override
    public Loader<NewsFeedPayload> onCreateLoader(int id, Bundle args) {
        return new NewsFeedLoader(getActivity(), sectionNumber);
    }

    @Override
    public void onLoadFinished(Loader<NewsFeedPayload> loader, NewsFeedPayload data) {
        Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@loaded "+isLoading);
//        Toast.makeText(getActivity(), "Loaded posts. size = "+data.size(), Toast.LENGTH_SHORT).show();
        ((FeedAdapter)feedRecyclerView.getAdapter()).setPosts(getActivity(), false, data.posts);
        feedRecyclerView.getAdapter().notifyDataSetChanged();
        isLoading = false;
        Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@loaded2 "+isLoading);
    }

    @Override
    public void onLoaderReset(Loader<NewsFeedPayload> loader) {
        loader.reset();
    }
}
