package za.ac.myuct.klmedu001.ivn.ivotenaija.loader;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.grosner.dbflow.sql.builder.Condition;
import com.grosner.dbflow.sql.language.Select;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.postApi.PostApi;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.NewsFeedPayload;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PostContainer;

/**
 * Created by eduardokolomajr on 2015/01/12.
 */
public class NewsFeedLoader extends AsyncTaskLoader<NewsFeedPayload> {
    public static final String TAG = "NFLoader";
    private final int loadType;
    private long fromTimestamp;
    private List<PostContainer> postItems;
    private List<PostContainer> postItemsToDeliver;
    private boolean isConnected = false;
    private final List<EntitiesFollowing> following;
    private boolean noMoreToLoad = false;
    private boolean top = false;
    private boolean delivered;
    
    /**
     * Stores away the application context associated with context. Since Loaders can be used
     * across multiple activities it's dangerous to store the context directly.
     *
     * @param context used to retrieve the application context.
     * @param loadType the type of posts to load (Party or Candidates)
     */
    public NewsFeedLoader(Context context, int loadType) {
        super(context);
        this.loadType = loadType;
        fromTimestamp = -1;

        Log.d(TAG, "const NFLoader start");

        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        following = new Select().from(EntitiesFollowing.class)
                .where(Condition.column(EntitiesFollowing$Table.TYPE).is(loadType)).queryList();

        postItems = new ArrayList<>();
        postItemsToDeliver = new ArrayList<>();

        Log.d(TAG, "const NFLoader end");
    }

    @Override
    public NewsFeedPayload loadInBackground() {
        Log.d(TAG, "loadingInBG "+fromTimestamp);

        if(postItems.size() > 0) {
            Log.d(TAG, "load in BG returning without loading more");
            return new NewsFeedPayload(top, postItems);
        }

        PostApi postApiService = IVNConstants.postEndpoint.build();

        List<PostContainer> posts = new ArrayList<>();

        //Load data if first time
        if(fromTimestamp < 0){
            for(EntitiesFollowing entity : following){
                Log.d(TAG, "attempting to load posts");
                try {
                    posts.addAll(
                            PostContainer.fromPostList(
                                    postApiService.getPostsByUser(entity.getId(), (long) entity.getType())
                                            .execute().getItems()
                            )
                    );
                }catch(IOException e){
                    throw new RuntimeException(e);
                }
            }
        }else if(postItems.size() < 20 && !noMoreToLoad){//load data from specific timestamp if loading more TODO refactor this to simply use a timestamp (ie remove else statement)
            for(EntitiesFollowing entity : following){
                try {
                    posts.addAll(
                            PostContainer.fromPostList(
                                    postApiService.getPostsByUserAfterTimestamp(fromTimestamp, entity.getId(), (long) entity.getType())
                                            .execute().getItems()
                            )
                    );
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }

        Collections.sort(posts);
        return new NewsFeedPayload(top, posts);
    }

    /********************************************************/
    /** (2) Deliver the results to the registered listener **/
    /**
     * ****************************************************
     */

    @Override
    public void deliverResult(NewsFeedPayload data) {
        Log.d(TAG, "delivering result = "+data.posts.size());

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
        if(postItems.size() == 0)
            postItems.addAll(data.posts);

        //Clear delivery list before adding next batch
        postItemsToDeliver.clear();

        if(postItems.size() > 19)
            postItemsToDeliver.addAll(postItems.subList(0,20));
        else
            postItemsToDeliver.addAll(postItems);

        for (int i=0; i<20; ++i){
            if(postItems.isEmpty())
                break;

            postItems.remove(0);
        }

        Log.d(TAG, "loader state = "+isStarted());

        if (isStarted() && !delivered) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            Log.d(TAG, "dev res = "+postItemsToDeliver.size());
            delivered = true;
            super.deliverResult(new NewsFeedPayload(top, postItemsToDeliver));
        }
    }

    /*********************************************************/
    /** (3) Implement the Loader’s state-dependent behavior **/
    /**
     * *****************************************************
     */

    @Override
    protected void onStartLoading() {
        Log.d(TAG, "starting loading");
        if (postItems != null && postItems.size() > 0) {
            // Deliver any previously loaded data immediately.
            Log.d(TAG, "SL delivering results");
            deliverResult(new NewsFeedPayload(top, postItems));
        }
        Log.d(TAG, "SL cont");

        // Begin monitoring the underlying data source.
//        if (mObserver == null) {
//            mObserver = new SampleObserver();
//            // TODO: register the observer
//        }

        if (takeContentChanged() || postItems.size() == 0) {
            // When the observer detects a change, it should call onContentChanged()
            // on the Loader, which will cause the next call to takeContentChanged()
            // to return true. If this is ever the case (or if the current data is
            // null), we force a new load.
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        cancelLoad();

        // Note that we leave the observer as is. Loaders in a stopped state
        // should still monitor the data source for changes so that the Loader
        // will know to force a new load if it is ever started again.
    }

    @Override
    protected void onReset() {
        // Ensure the loader has been stopped.
        onStopLoading();
        Log.d(TAG, "resetting");

        // At this point we can release the resources associated with 'postItems'.
        if (postItems != null && postItems.size() > 0) {
            Log.d(TAG, "resetting with posts still left = "+postItems.size());
            releaseResources(postItemsToDeliver);
        }

        // The Loader is being reset, so we should stop monitoring for changes.
//        if (mObserver != null) {
//            // TODO: unregister the observer
//            mObserver = null;
//        }

        delivered = false;
    }

    @Override
    public void onCanceled(NewsFeedPayload data) {
        // Attempt to cancel the current asynchronous load.
        super.onCanceled(data);

        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        if(data.posts != null)
            releaseResources(data.posts);
    }

    private void releaseResources(List<PostContainer> data) {
        // For a simple List, there is nothing to do. For something like a Cursor, we
        // would close it in this method. All resources associated with the Loader
        // should be released here.
        if (data != null)
            data.clear();
    }

}
