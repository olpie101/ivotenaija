package za.ac.myuct.klmedu001.ivn.ivotenaija.constant;

import android.database.sqlite.SQLiteDatabase;

import com.grosner.dbflow.annotation.Migration;
import com.grosner.dbflow.sql.migration.BaseMigration;

/**
 * Created by eduardokolomajr on 2014/12/03.
 * Initial Migration
 */
@Migration(version = AppDatabase.VERSION, databaseName = AppDatabase.NAME)
public class DatabaseMigration0 extends BaseMigration{
    @Override
    public void onPreMigrate() {
            super.onPreMigrate();
    }

    @Override
    public void migrate(SQLiteDatabase sqLiteDatabase) {

    }
}
