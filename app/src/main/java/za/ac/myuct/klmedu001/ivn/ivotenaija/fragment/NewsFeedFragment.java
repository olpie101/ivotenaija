package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.grosner.dbflow.sql.language.Select;
import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import za.ac.myuct.klmedu001.ivn.ivotenaija.InfoActivity;
import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.BaseApplication;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter.NewsFeedAdapter;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter.SearchAdapter;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.InfoClickedEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.SectionAttachedEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.view.SlidingTabLayout;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsFeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFeedFragment extends Fragment implements View.OnFocusChangeListener, SearchView.OnQueryTextListener, SearchView.OnSuggestionListener{
    private final String TAG = "NewsFeedFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";
    private int currentTab = 0;

    @InjectView(R.id.news_feed_tabs)
    SlidingTabLayout tabs;
    @InjectView(R.id.vp_news_feed)
    ViewPager tabsPager;
    @InjectView(R.id.fab_add)
    AddFloatingActionButton fab;
    SearchView searchView;
    MenuItem searchItem;



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment NewsFeedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFeedFragment newInstance(int sectionNumber) {
        NewsFeedFragment fragment = new NewsFeedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public NewsFeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news_feed, container, false);

        ButterKnife.inject(this, v);

        Log.d(TAG, "setAdapter");
        tabsPager.setAdapter(new NewsFeedAdapter(getChildFragmentManager(), getActivity()));

        Log.d(TAG, "setViewPager");
        tabs.setViewPager(tabsPager);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_feed, menu);
        searchItem = menu.findItem(R.id.action_add);
        searchView = (SearchView) menu.findItem(R.id.action_add).getActionView();
        searchView.setQueryHint("Party/Candidate Name");
        searchView.setOnQueryTextFocusChangeListener(this);
        searchView.setOnQueryTextListener(this);
        searchView.setOnSuggestionListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(getArguments() != null && getArguments().getInt(ARG_SECTION_NUMBER) > 0)
            BaseApplication.getEventBus().post(new SectionAttachedEvent(getArguments()
                    .getInt(ARG_SECTION_NUMBER)));
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseApplication.getEventBus().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        BaseApplication.getEventBus().unregister(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Log.d(TAG, "vid = "+v.getId());
        Log.d(TAG, "aid = "+R.id.action_add);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        currentTab = tabsPager.getCurrentItem();
        switch (currentTab) {
            case IVNConstants.FEED_PARTIES_ID:
                Cursor partiesCursor = new Select().from(PartyContainer.class)
                        .where("name LIKE '" + s + "%'").query();
                Log.d(TAG, "pquery size = "+partiesCursor.getCount());
                searchView.setSuggestionsAdapter(new SearchAdapter(getActivity(), partiesCursor,
                        0, R.layout.search_result, currentTab, PartyContainer$Table.NAME));
                break;
            case IVNConstants.FEED_CANDIDATES_ID:
                Cursor candidatesCursor = new Select().from(CandidateContainer.class)
                        .where("name LIKE '" + s + "%'").query();
                searchView.setSuggestionsAdapter(new SearchAdapter(getActivity(), candidatesCursor,
                        0, R.layout.search_result, currentTab, CandidateContainer$Table.NAME));
                break;

        }
        return true;
    }

    @Override
    public boolean onSuggestionSelect(int i) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int i) {
        Log.d(TAG, "Suggestion clicked");
        return true;
    }

    @OnClick(R.id.fab_add)
    public void onFabClick(){
        if(!searchItem.isActionViewExpanded())
            searchItem.expandActionView();
    }

    @Subscribe
    public void onInfoClickedEventReceived(InfoClickedEvent evt){
        Intent intent = new Intent(this.getActivity(), InfoActivity.class);
        intent.putExtra(InfoActivity.EXTRA_TYPE, evt.type);
        intent.putExtra(InfoActivity.EXTRA_ID, evt.id);
        startActivity(intent);
    }
}


