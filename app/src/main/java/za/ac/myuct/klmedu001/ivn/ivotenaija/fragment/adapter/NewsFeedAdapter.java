package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.FeedFragment;

/**
 * Created by eduardokolomajr on 2014/11/16.
 * News Feed Adapter
 * Used to manage both party and candidate news feed
 */
public class NewsFeedAdapter extends FragmentPagerAdapter {
    private final String TAG = "NEWS_FEED_ADAPTER";

    private final String TITLE_PARTIES;
    private final String TITLE_CANDIDATES;

    public NewsFeedAdapter(FragmentManager childFragmentManager, Context context) {
        super(childFragmentManager);
        TITLE_PARTIES = context.getResources().getString(R.string.title_party_feed);
        TITLE_CANDIDATES = context.getResources().getString(R.string.title_candidate_feed);
    }

    /**
     * @return the number of pages to display
     */
    @Override
    public int getCount() {
        return 2;
    }

    /**
     * Return the title of the item at {@code position}. This is important as what this method
     * returns is what is displayed in the {@link za.ac.myuct.klmedu001.ivn.ivotenaija.view.SlidingTabLayout}.
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case IVNConstants.FEED_PARTIES_ID:
                return TITLE_PARTIES;
            case IVNConstants.FEED_CANDIDATES_ID:
                return TITLE_CANDIDATES;
        }
        return "";
    }

    @Override
    public Fragment getItem(int position) {
        if (position == IVNConstants.FEED_PARTIES_ID){
            return FeedFragment.newInstance(FeedFragment.PARTIES);
        }else{
            return FeedFragment.newInstance(FeedFragment.CANDIDATES);
        }
    }

}
