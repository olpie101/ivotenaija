package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.candidateTypeApi.model.CandidateType;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;


/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for {@link za.ac.myuct.klmedu001.ivn.api.entity.candidateTypeApi.model.CandidateType}
 * presidential = 1
 * gubernatorial = 2        states
 * senatorial = 3           states
 * house of reps = 4        districts
 * state house of ass = 5   states
 * chairmanship = 6         lga
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_CANDIDATE_TYPE)
public class CandidateTypeContainer extends BaseModel {
    @Column(columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column String name;

    public CandidateTypeContainer() {
    }

    public CandidateTypeContainer(Long id, String name) {
        _id = id;
        this.name = name;
    }

    public CandidateTypeContainer(CandidateType candidateType){
        this._id = candidateType.getId();
        this.name = candidateType.getName();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CandidateType getCandidateType(){
        CandidateType candidateType = new CandidateType();
        candidateType.setId(_id);
        candidateType.setName(name);
        return candidateType;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.candidateTypeApi.model.CandidateType} to a
     * list of {@code CandidateTypeContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code Candidate}s which is to be converted
     * @return converted list
     */
    public static List<CandidateTypeContainer> fromCandidateTypeList(List<CandidateType> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<CandidateTypeContainer> containerList = new ArrayList<>(list.size());

        for (CandidateType candidateType : list) {
            containerList.add(new CandidateTypeContainer(candidateType));
        }
        return containerList;
    }
}
