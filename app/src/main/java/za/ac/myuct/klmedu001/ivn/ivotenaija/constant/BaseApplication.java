package za.ac.myuct.klmedu001.ivn.ivotenaija.constant;

import android.app.Application;
import android.util.Log;

import com.grosner.dbflow.config.FlowManager;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by eduardokolomajr on 2014/11/15.
 *
 * This is the base application used main for Otto Bus
 */
public class BaseApplication extends Application {
    private static final String TAG = "BaseApplication";
    private static Bus mEventBus;

    public static Bus getEventBus() {
        return mEventBus;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "application started");
        mEventBus = new Bus(ThreadEnforcer.ANY);
        FlowManager.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        FlowManager.destroy();
    }
}
