package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.districtApi.model.District;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;


/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for District entity
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_DISTRICT)
public class DistrictContainer extends BaseModel{
    @Column (columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column String name;

    @Column long stateId;

    public DistrictContainer() {
    }

    public DistrictContainer(Long id, String name, long stateId) {
        _id = id;
        this.name = name;
        this.stateId = stateId;
    }

    public DistrictContainer(District district){
        this._id = district.getId();
        this.name = district.getName();
        this.stateId = district.getStateId();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStateId() {
        return stateId;
    }

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }

    public District getDistrict(){
        District district = new District();
        district.setId(_id);
        district.setName(name);
        district.setStateId(stateId);
        return district;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.districtApi.model.District} to a
     * list of {@code DistrictContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code District} which is to be converted
     * @return converted list
     */
    public static List<DistrictContainer> fromDistrictList(List<District> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<DistrictContainer> containerList = new ArrayList<>(list.size());

        for (District country : list) {
            containerList.add(new DistrictContainer(country));
        }
        return containerList;
    }
}
