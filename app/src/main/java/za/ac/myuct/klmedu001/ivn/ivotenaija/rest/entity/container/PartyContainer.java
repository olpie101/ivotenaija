package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.partyApi.model.Party;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for Party entity
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_PARTY)
public class PartyContainer extends BaseModel {
    @Column(columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column String name;
    @Column String motto;
    @Column String logoPath;
    @Column String info;

    public PartyContainer() {
    }

    @SuppressWarnings("UnusedDeclaration")
    public PartyContainer(Long id, String name, String motto, String logoPath, String info) {
        _id = id;
        this.name = name;
        this.motto = motto;
        this.logoPath = logoPath;
        this.info = info;
    }
    
    public PartyContainer (Party party){
        this._id = party.getId();
        this.name = party.getName();
        this.motto = party.getMotto();
        this.logoPath = party.getLogoPath();
        this.info = party.getInfo();
    }

    public Long getId() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.partyApi.model.Party} to a
     * list of {@code PartyContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code Party} which is to be converted
     * @return converted list
     */
    public static List<PartyContainer> fromPartyList(List<Party> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<PartyContainer> containerList = new ArrayList<>(list.size());

        for (Party country : list) {
            containerList.add(new PartyContainer(country));
        }
        return containerList;
    }
}
