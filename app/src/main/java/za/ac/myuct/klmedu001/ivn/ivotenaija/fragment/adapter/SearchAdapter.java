package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thedazzler.droidicon.badges.DroidiconBadge;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.BaseApplication;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.InfoClickedEvent;

/**
 * Created by eduardokolomajr on 2014/12/08.
 * Adapter for search view used to find candidates and parties
 */
public class SearchAdapter extends CursorAdapter {
    int layout;
    int type;
    String columnName;

    public SearchAdapter(Context context, Cursor c, int flags, int layout, int type, String columnName) {
        super(context, c, flags);
        this.layout = layout;
        this.type = type;
        this.columnName = columnName;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setTag(vh);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        int nameColumnIndex = cursor.getColumnIndex(columnName);
        int idColumnIndex = cursor.getColumnIndex("_id");
        viewHolder.text.setText(cursor.getString(nameColumnIndex));
        viewHolder.id = cursor.getInt(idColumnIndex);
    }

    public class ViewHolder{
        public static final String TAG = "SearchAdapter.ViewHolder";
        int id;
        @InjectView(R.id.tv_search_result)
        TextView text;
        @InjectView(R.id.di_search_result)
        DroidiconBadge info;

        public ViewHolder(View v){
            ButterKnife.inject(this, v);
        }

        @OnClick(R.id.tv_search_result)
        public void onTextClicked(){
            Log.d(TAG, "text clicked");
        }

        @OnClick(R.id.di_search_result)
        public void onInfoClicked(){
            Log.d(TAG, "info clicked");
            BaseApplication.getEventBus().post(new InfoClickedEvent(type, id));
        }
    }
}
