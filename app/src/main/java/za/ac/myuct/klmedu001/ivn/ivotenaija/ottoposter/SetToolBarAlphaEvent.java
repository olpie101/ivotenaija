package za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter;

/**
 * Created by eduardokolomajr on 2015/01/07.
 * used to set the toolbar alpha state event
 */
public class SetToolBarAlphaEvent {
    public final float alpha;

    public SetToolBarAlphaEvent(float alpha) {
        this.alpha = alpha;
    }
}
