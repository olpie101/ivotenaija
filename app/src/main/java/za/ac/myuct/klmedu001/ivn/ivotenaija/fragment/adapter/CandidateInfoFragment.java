package za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter;


import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.grosner.dbflow.sql.builder.Condition;
import com.grosner.dbflow.sql.language.Select;
import com.thedazzler.droidicon.badges.DroidiconBadge;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing;
import za.ac.myuct.klmedu001.ivn.ivotenaija.EntitiesFollowing$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.R;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.BaseApplication;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.SetToolBarAlphaEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateTypeContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateTypeContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.DistrictContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.DistrictContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.LGAContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.LGAContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer$Table;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.StateContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.StateContainer$Table;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Use the {@link za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter.CandidateInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CandidateInfoFragment extends Fragment implements ViewTreeObserver.OnScrollChangedListener{
    public static final String TAG = CandidateInfoFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String EXTRA_ID = "id";

    private int id;
    private CandidateContainer candidate;
    private PartyContainer party;
    private CandidateTypeContainer candidateType;
    private DistrictContainer district;
    private LGAContainer lga;
    private StateContainer state;
    private boolean following = false;

    @InjectView(R.id.iv_hero_image_candidate)
    ImageView heroImage;
    @InjectView(R.id.sv_info_main_scroll)
    ScrollView mainScroll;
    @InjectView(R.id.tv_hero_name)
    TextView heroName;
    @InjectView(R.id.tv_hero_party_name)
    TextView heroPartyName;
    @InjectView(R.id.tv_hero_running_for_position)
    TextView heroRunningForPosition;
    @InjectView(R.id.fab_follow)
    FrameLayout fabFollow;
    @InjectView(R.id.fab_follow_bg)
    View fabBackground;
    @InjectView(R.id.fab_follow_add)
    DroidiconBadge fabAddIcon;
    @InjectView(R.id.fab_follow_tick)
    DroidiconBadge fabFollowingIcon;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id id of candidate to display.
     * @return A new instance of fragment CandidateInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CandidateInfoFragment newInstance(int id) {
        CandidateInfoFragment fragment = new CandidateInfoFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public CandidateInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt(EXTRA_ID);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_candidate, container, false);
        ButterKnife.inject(this, view);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mainScroll.getViewTreeObserver().addOnScrollChangedListener(this);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        heroName.setText(candidate.getName());
        heroPartyName.setText(party.getName());

        //Set the text which displays what the candidate is running for
        String runningForText = candidateType.getName()+" candidate ";

        //Add this is if candidate is not presidential, to add the region where they are running for
        if(candidate.getRunningFor() > 1)
            runningForText += "in ";

        switch ((int)candidate.getRunningFor()){
            case 2:
            case 3:
            case 5:
                runningForText += state.getName();
                break;
            case 4:
                runningForText += district.getName();
                break;
            case 6:
                runningForText += lga.getName();
                break;
            default:
                break;
        }
        heroRunningForPosition.setText(runningForText);


        setFabState();
    }

    //Runs the required database queries in order to get the correct information
    private void init(){
        Log.d(TAG, "about to get single entry from db with id = "+id);
        candidate = new Select().from(CandidateContainer.class)
                .where(Condition.column(CandidateContainer$Table._ID).is(id)).querySingle();
        party = new Select().from(PartyContainer.class)
                .where(Condition.column(PartyContainer$Table._ID).is(candidate.getPartyID())).querySingle();
        candidateType = new Select().from(CandidateTypeContainer.class)
                .where(Condition.column(CandidateTypeContainer$Table._ID).is(candidate.getRunningFor())).querySingle();

        EntitiesFollowing currentEntity = new Select().from(EntitiesFollowing.class)
                .where(Condition.column(EntitiesFollowing$Table.TYPE).is(IVNConstants.FEED_CANDIDATES_ID))
                .and(Condition.column(EntitiesFollowing$Table.ID).is(id)).querySingle();

        if (currentEntity != null ) {
            Log.d(TAG, "user is following");
            following = true;
        }else{
            Log.d(TAG, "user not is following");
        }

        long regionId = candidate.getRegionId();
        switch ((int)candidate.getRunningFor()){
            case 2:
            case 3:
            case 5:
                state = new Select().from(StateContainer.class)
                        .where(Condition.column(StateContainer$Table._ID).is(regionId)).querySingle();
                break;
            case 4:
                district = new Select().from(DistrictContainer.class)
                        .where(Condition.column(DistrictContainer$Table._ID).is(regionId)).querySingle();
                break;
            case 6:
                lga = new Select().from(LGAContainer.class)
                        .where(Condition.column(LGAContainer$Table._ID).is(regionId)).querySingle();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.fab_follow)
    public void fabFollowClicked (){
        Log.d(TAG, "fabClicked");
        toggleFollowing();
        setFabState();
    }

    private void setFabState(){
        Drawable d;

        if(following) {
            fabAddIcon.setAlpha(0f);
            fabFollowingIcon.setAlpha(1f);
            d = getResources().getDrawable(R.drawable.circle_accent);
        }else{
            fabAddIcon.setAlpha(1f);
            fabFollowingIcon.setAlpha(0f);
            d = getResources().getDrawable(R.drawable.circle_primary_dark);
        }

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
            fabBackground.setBackgroundDrawable(d);
        }else{
            fabBackground.setBackground(d);
        }
    }

    private void toggleFollowing(){
        following = !following;

        if (!following) {
            Log.d(TAG, "deleting type = "+IVNConstants.FEED_CANDIDATES_ID+" id = "+id);
            EntitiesFollowing followingItem = new Select().from(EntitiesFollowing.class)
                    .where(Condition.column(EntitiesFollowing$Table.TYPE)
                            .is(IVNConstants.FEED_CANDIDATES_ID))
                    .and(Condition.column(EntitiesFollowing$Table.ID).is(id)).querySingle();
            if(followingItem != null) {
                followingItem.delete(true);
                Log.d(TAG, "deleted item with id = "+followingItem.getId());
            }
        }else{
            Log.d(TAG, "inserting type = "+IVNConstants.FEED_CANDIDATES_ID+" id = "+id);
            EntitiesFollowing newEntry = new EntitiesFollowing(IVNConstants.FEED_CANDIDATES_ID, id);
            newEntry.save(true);
        }

    }


    @Override
    public void onScrollChanged() {
        int scrollY = mainScroll.getScrollY();
        float heroImageHeight = heroImage.getHeight();
        Log.d(TAG, "main scroll y = "+scrollY);

        //Used to get actionbar height
        final TypedArray styledAttributes = getActivity().getBaseContext().getTheme()
                .obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int actionbarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        Log.d(TAG, "actionbarsize  = "+actionbarSize);
        float alpha = (scrollY/(heroImageHeight-actionbarSize));
        BaseApplication.getEventBus().post(new SetToolBarAlphaEvent(Math.min(alpha, 1)));
    }


}
