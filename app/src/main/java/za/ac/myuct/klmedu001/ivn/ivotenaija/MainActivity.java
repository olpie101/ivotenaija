package za.ac.myuct.klmedu001.ivn.ivotenaija;

import android.animation.ValueAnimator;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.grosner.dbflow.runtime.TransactionManager;
import com.grosner.dbflow.runtime.transaction.process.ProcessModelInfo;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import za.ac.myuct.klmedu001.ivn.api.entity.candidateApi.CandidateApi;
import za.ac.myuct.klmedu001.ivn.api.entity.candidateTypeApi.CandidateTypeApi;
import za.ac.myuct.klmedu001.ivn.api.entity.districtApi.DistrictApi;
import za.ac.myuct.klmedu001.ivn.api.entity.lGAApi.LGAApi;
import za.ac.myuct.klmedu001.ivn.api.entity.lastUpdateApi.LastUpdateApi;
import za.ac.myuct.klmedu001.ivn.api.entity.lastUpdateApi.model.LastUpdate;
import za.ac.myuct.klmedu001.ivn.api.entity.partyApi.PartyApi;
import za.ac.myuct.klmedu001.ivn.api.entity.stateApi.StateApi;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.BaseApplication;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.NavigationDrawerFragment;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.NewsFeedFragment;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.CurrentToolBarAlphaEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.SectionAttachedEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.CandidateTypeContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.DistrictContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.LGAContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.PartyContainer;
import za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container.StateContainer;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, ValueAnimator.AnimatorUpdateListener {
    private final String TAG = "MainActivity";
    @InjectView(R.id.main_toolbar)
    Toolbar actionBar;
    private int oldPosition = -1;
    private Drawable actionbarDrawable;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        ButterKnife.inject(this);

        actionbarDrawable = getResources().getDrawable(R.color.primary);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
            Log.d(TAG, "SBD");
            actionBar.setBackgroundDrawable(actionbarDrawable);
        } else {
            Log.d(TAG, "SB");
            actionBar.setBackground(actionbarDrawable);
        }

        setSupportActionBar(actionBar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        //don't change fragment if same position
        if (position == oldPosition)
            return;
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        Log.d(TAG, "frag count=" + fragmentManager.getFragments().size());
        switch (position) {
            case 0:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, NewsFeedFragment.newInstance(position))
                        .commit();
                break;
//            case 1:
//                fragmentManager.beginTransaction()
//                        .replace(R.id.container, JammieFragment.newInstance(position))
//                        .commit();
//                break;
        }
        oldPosition = position;
        Log.d(TAG, "fragment replaced");
    }

    //TODO refactor this to just change the title from within the fragment
    @Subscribe
    public void onSectionAttached(SectionAttachedEvent evt) {
        switch (evt.SECTION_NUMBER) {
            case 0:
                mTitle = getString(R.string.title_party_feed);
                setTitle(mTitle);
                break;
            case 1:
                mTitle = getString(R.string.title_candidate_feed);
                break;
        }
        setTitle(mTitle);
    }

    public void restoreActionBar() {
        setSupportActionBar(actionBar);
        setTitle(mTitle);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseApplication.getEventBus().unregister(this);
        Log.d(TAG, "pausing");
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseApplication.getEventBus().register(this);
        new UpdateCoreData().execute();
        Log.d(TAG, "####################resuming");
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        int currentValue = (int) animation.getAnimatedValue();
        actionbarDrawable.setAlpha(currentValue);
    }

    @Subscribe
    public void resetToolbarAlpha (CurrentToolBarAlphaEvent evt){
        if (evt.alpha < 255) {
            ValueAnimator toolbarAlphaAnimator = ValueAnimator.ofInt(evt.alpha, 255);
            toolbarAlphaAnimator.setDuration(100);
            toolbarAlphaAnimator.addUpdateListener(this);
            toolbarAlphaAnimator.start();
        }
    }

    class UpdateCoreData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.d(TAG, "##############updating core data");
            SharedPreferences prefs = getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

            LastUpdateApi lastUpdateEndpointService = IVNConstants.lastUpdateEndpoint.build();

            try {
                List<LastUpdate> lastUpdates = lastUpdateEndpointService.list().execute().getItems();
                for (LastUpdate lastUpdate : lastUpdates) {
                    long currentUpdateTimestamp = prefs.getLong(lastUpdate.getName(), 0);
                    Log.d(TAG, "##############"+lastUpdate.getName()+" => current time = "+currentUpdateTimestamp+" vs "+lastUpdate.getTimestamp());
                    if (currentUpdateTimestamp < lastUpdate.getTimestamp()) {
                        try {
                            switch (lastUpdate.getName()) {
                                case IVNConstants.LAST_UPDATE_CANDIDATE:
                                    updateCandidates(lastUpdate.getTimestamp());
                                    break;
                                case IVNConstants.LAST_UPDATE_CANDIDATE_TYPE:
                                    updateCandidateTypes(lastUpdate.getTimestamp());
                                    break;
                                case IVNConstants.LAST_UPDATE_PARTY:
                                    updateParties(lastUpdate.getTimestamp());
                                    break;
                                case IVNConstants.LAST_UPDATE_STATE:
                                    updateStates(lastUpdate.getTimestamp());
                                    break;
                                case IVNConstants.LAST_UPDATE_DISTRICT:
                                    updateDistrict(lastUpdate.getTimestamp());
                                    break;
                                case IVNConstants.LAST_UPDATE_LGA:
                                    updateLGAs(lastUpdate.getTimestamp());
                                    break;
                            }
                        } catch (IOException e) {
                            Log.d(TAG + " - UpdateCore", "Error updating " + lastUpdate.getName());
                        }
                    }
                }
            } catch (IOException e) {
                Log.d(TAG + " - UpdateCore", "Error getting last updates");
            }
            return null;
        }

        private void updateLGAs(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating LGAs");

            LGAApi districtEndpointService = IVNConstants.LGAEndpoint.build();

            List<LGAContainer> lgaContainerList = new ArrayList<>();

            lgaContainerList.addAll(LGAContainer.fromLGAList(
                    districtEndpointService.getByCountryId(IVNConstants.COUNTRY_ID)
                            .execute().getItems()));

            if (lgaContainerList.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(lgaContainerList));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_LGA, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating LGAs");
        }

        private void updateDistrict(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating districts");

            DistrictApi districtEndpointService = IVNConstants.districtEndpoint.build();

            List<DistrictContainer> districtContainerList = new ArrayList<>();

            districtContainerList.addAll(DistrictContainer.fromDistrictList(
                    districtEndpointService.getByCountryId(IVNConstants.COUNTRY_ID)
                            .execute().getItems()));

            if (districtContainerList.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(districtContainerList));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_DISTRICT, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating district");
        }

        private void updateStates(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating states");

            StateApi stateEndpointService = IVNConstants.stateEndpoint.build();

            List<StateContainer> stateContainerList = new ArrayList<>();

            stateContainerList.addAll(StateContainer.fromStateList(stateEndpointService
                    .getStatesByCountryId(IVNConstants.COUNTRY_ID).execute().getItems()));

            if (stateContainerList.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(stateContainerList));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_STATE, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating states");
        }

        private void updateParties(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating parties");

            PartyApi partyEndpointService = IVNConstants.partyEndpoint.build();

            List<PartyContainer> partyListContainer = new ArrayList<>();

            partyListContainer.addAll(PartyContainer.fromPartyList(partyEndpointService
                    .getByCountryId(IVNConstants.COUNTRY_ID).execute().getItems()));

            if (partyListContainer.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(partyListContainer));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_PARTY, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating parties");
        }

        private void updateCandidateTypes(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating candidate types");

            CandidateTypeApi candidateTypesEndpointService = IVNConstants.candidateTypeEndpoint.build();

            List<CandidateTypeContainer> candidateTypeContainerList = new ArrayList<>();

            candidateTypeContainerList.addAll(CandidateTypeContainer.fromCandidateTypeList(
                    candidateTypesEndpointService.getCandidateTypesByCountryId(IVNConstants.COUNTRY_ID)
                            .execute().getItems()));

            if (candidateTypeContainerList.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(candidateTypeContainerList));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_CANDIDATE_TYPE, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating candidate types");
        }

        private void updateCandidates(long newTimestamp) throws IOException {
            Log.d(TAG + " - Update Core", "Updating candidates");

            CandidateApi candidatesEndpointService = IVNConstants.candidateEndpoint.build();

            List<CandidateContainer> candidateContainerList = new ArrayList<>();

            candidateContainerList.addAll(
                    CandidateContainer.fromCandidateList(candidatesEndpointService
                            .getByCountryId(IVNConstants.COUNTRY_ID).execute().getItems()));

            if (candidateContainerList.size() > 0) {
                TransactionManager.getInstance().save(ProcessModelInfo.withModels(candidateContainerList));

                SharedPreferences prefs =
                        getSharedPreferences(IVNConstants.PREFS_LAST_UPDATE, MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(IVNConstants.LAST_UPDATE_CANDIDATE, newTimestamp);
                editor.apply();
            } else
                throw new IOException("Error updating candidates");
        }


    }
}
