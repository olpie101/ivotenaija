package za.ac.myuct.klmedu001.ivn.ivotenaija;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.BaseApplication;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.PartyInfoFragment;
import za.ac.myuct.klmedu001.ivn.ivotenaija.fragment.adapter.CandidateInfoFragment;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.CurrentToolBarAlphaEvent;
import za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter.SetToolBarAlphaEvent;


public class InfoActivity extends ActionBarActivity implements ValueAnimator.AnimatorUpdateListener {
    public static final String TAG = "InfoActivity";
    public static final String EXTRA_TYPE = "type";
    public static final String EXTRA_ID = "id";

    private int type;
    private int id;

    @InjectView(R.id.main_toolbar)
    Toolbar actionBar;

    Drawable actionbarDrawable;
    int currentAlpha = 0;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            type = intent.getIntExtra(EXTRA_TYPE, 0);
            id = intent.getIntExtra(EXTRA_ID, 0);
        }

        switch(type){
            case IVNConstants.FEED_PARTIES_ID:
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, PartyInfoFragment.newInstance(id))
                        .commit();
                break;
            case IVNConstants.FEED_CANDIDATES_ID:
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, CandidateInfoFragment.newInstance(id))
                        .commit();
                break;
            default:
                break;
        }

        ButterKnife.inject(this);

        actionbarDrawable = getResources().getDrawable(R.color.primary);
        actionbarDrawable.setAlpha(0);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
            actionBar.setBackgroundDrawable(actionbarDrawable);
        } else {
            actionBar.setBackground(actionbarDrawable);
        }

        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseApplication.getEventBus().register(this);
        Log.d(TAG, "resuming");
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseApplication.getEventBus().unregister(this);
        Log.d(TAG, "pausing");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "destroying");
        BaseApplication.getEventBus().post(new CurrentToolBarAlphaEvent(currentAlpha));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void onSetToolBarAlpha(SetToolBarAlphaEvent evt){
//        actionBar.setAlpha(evt.alpha);
        currentAlpha = (int) (evt.alpha * 255);
        Log.d(TAG, "updating toolbar a = "+currentAlpha);
        actionbarDrawable = actionBar.getBackground();
        actionbarDrawable.setAlpha(currentAlpha);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
            actionBar.setBackgroundDrawable(actionbarDrawable);
        } else {
            actionBar.setBackground(actionbarDrawable);
        }
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        Log.d(TAG, "anim update"+((int)animation.getAnimatedValue()));
        BaseApplication.getEventBus().post(new SetToolBarAlphaEvent((int) animation.getAnimatedValue()));
    }
}
