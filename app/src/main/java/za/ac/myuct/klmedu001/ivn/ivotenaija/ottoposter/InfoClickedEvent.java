package za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter;

/**
 * Created by eduardokolomajr on 2014/12/08.
 * Fired when info button is hit in the search results from search view
 *
 */
public class InfoClickedEvent {
    public final int type;
    public final int id;

    public InfoClickedEvent(int type, int id) {
        this.type = type;
        this.id = id;
    }
}
