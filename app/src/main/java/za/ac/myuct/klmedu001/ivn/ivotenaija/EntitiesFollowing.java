package za.ac.myuct.klmedu001.ivn.ivotenaija;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.IVNConstants;

/**
 * Created by eduardokolomajr on 2014/12/05.
 * Used to keep track of who user follows
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_FOLLOWING)
public class EntitiesFollowing extends BaseModel{
    public static final int PARTIES = IVNConstants.FEED_PARTIES_ID;
    public static final int CANDIDATES = IVNConstants.FEED_CANDIDATES_ID;

    @Column(columnType = Column.PRIMARY_KEY)
    int type;
    @Column(columnType = Column.PRIMARY_KEY)
    long id;

    public EntitiesFollowing() {
    }

    public EntitiesFollowing(int type, long id) {
        this.type = type;
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
