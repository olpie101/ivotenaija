package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.stateApi.model.State;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for State entity
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_STATE)
public class StateContainer extends BaseModel{
    @Column(columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column String name;
    @Column long countryId;

    public StateContainer() {
    }

    public StateContainer(Long id, String name, long countryId) {
        _id = id;
        this.name = name;
        this.countryId = countryId;
    }

    public StateContainer(State state){
        this._id = state.getId();
        this.name = state.getName();
        this.countryId = state.getCountryId();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.stateApi.model.State} to a
     * list of {@code StateContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code State} which is to be converted
     * @return converted list
     */
    public static List<StateContainer> fromStateList(List<State> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<StateContainer> containerList = new ArrayList<>(list.size());

        for (State country : list) {
            containerList.add(new StateContainer(country));
        }
        return containerList;
    }
}
