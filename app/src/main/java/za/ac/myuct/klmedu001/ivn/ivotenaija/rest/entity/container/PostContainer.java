package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import android.support.annotation.NonNull;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.postApi.model.Post;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2015/01/12.
 * Container for the {@link Post} entity
 */
@SuppressWarnings("UnusedDeclaration")
@Table (databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_POST)
public class PostContainer extends BaseModel implements Comparable{
    @Column(columnType = Column.PRIMARY_KEY)
    long _id;
    @Column long userType;
    @Column long userId;
    @Column String title;
    @Column String content;
    @Column String picPath;
    @Column long timestamp;

    public PostContainer() {
    }

    public PostContainer(long _id, long userType, long userId, String title, String content, String picPath, long timestamp) {
        this._id = _id;
        this.userType = userType;
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.picPath = picPath;
        this.timestamp = timestamp;
    }

    public PostContainer (Post post){
        this._id = post.getId();
        this.userType = post.getUserType();
        this.userId = post.getUserId();
        this.title = post.getTitle();
        this.picPath = post.getPicPath();
        this.content = post.getContent();
        this.timestamp = post.getTimestamp();
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public long getUserType() {
        return userType;
    }

    public void setUserType(long userType) {
        this.userType = userType;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.postApi.model.Post} to a
     * list of {@code PostContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code Post} which is to be converted
     * @return converted list
     */
    public static List<PostContainer> fromPostList(List<Post> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<PostContainer> containerList = new ArrayList<>(list.size());

        for (Post post: list) {
            containerList.add(new PostContainer(post));
        }
        return containerList;
    }

    @Override
    public int compareTo(@NonNull Object other) {
        if(!(other instanceof PostContainer))
            throw new  IllegalArgumentException("Parameter other must be of type PostContainer");

        if(timestamp == ((PostContainer) other).timestamp)
            return 0;

        return (timestamp > ((PostContainer) other).timestamp) ? 1 : -1;
    }
}
