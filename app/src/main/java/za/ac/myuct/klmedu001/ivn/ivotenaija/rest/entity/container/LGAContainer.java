package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.lGAApi.model.LGA;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for LGA entity
 */
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_LGA)
public class LGAContainer extends BaseModel{
    @Column(columnType = Column.PRIMARY_KEY)
    Long _id;
    @Column String name;

    @Column long stateId;

    public LGAContainer() {
    }

    public LGAContainer(Long id, String name, long stateId) {
        _id = id;
        this.name = name;
        this.stateId = stateId;
    }

    public LGAContainer(LGA lga){
        this._id  = lga.getId();
        this.name = lga.getName();
        this.stateId = lga.getStateId();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStateId() {
        return stateId;
    }

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }

    public LGA getLGA(){
        LGA lga = new LGA();
        lga.setId(_id);
        lga.setName(name);
        lga.setStateId(stateId);
        return lga;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.lGAApi.model.LGA} to a
     * list of {@code LGAContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code LGA} which is to be converted
     * @return converted list
     */
    public static List<LGAContainer> fromLGAList(List<LGA> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<LGAContainer> containerList = new ArrayList<>(list.size());

        for (LGA lga : list) {
            containerList.add(new LGAContainer(lga));
        }
        return containerList;
    }
}
