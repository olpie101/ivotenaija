package za.ac.myuct.klmedu001.ivn.ivotenaija.ottoposter;

/**
 * Created by eduardokolomajr on 2014/11/16.
 */
public class SectionAttachedEvent {
    public final int SECTION_NUMBER;

    public SectionAttachedEvent(int SECTION_NUMBER) {
        this.SECTION_NUMBER = SECTION_NUMBER;
    }
}
