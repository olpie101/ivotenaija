package za.ac.myuct.klmedu001.ivn.ivotenaija.rest.entity.container;

import com.grosner.dbflow.annotation.Column;
import com.grosner.dbflow.annotation.Table;
import com.grosner.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import za.ac.myuct.klmedu001.ivn.api.entity.candidateApi.model.Candidate;
import za.ac.myuct.klmedu001.ivn.ivotenaija.constant.AppDatabase;

/**
 * Created by eduardokolomajr on 2014/12/02.
 * Container for {@link Candidate} class
 */
@SuppressWarnings("UnusedDeclaration")
@Table(databaseName = AppDatabase.NAME, value = AppDatabase.TABLE_CANDIDATE)
public class CandidateContainer extends BaseModel{
    @Column(columnType = Column.PRIMARY_KEY)
    long _id;
    @Column String name;
    @Column long DOB;
    @Column String POB;
    @Column String currentJob;
    @Column long runningFor;
    @Column long regionId;
    @Column long partyID;
    @Column int termNumber;
    @Column String personalInfo;
    @Column String manifesto;
    @Column String picPath;
    @Column long followers;

    public CandidateContainer() {
    }

    public CandidateContainer(long id, String name, long DOB, String POB, String currentJob,
                             long runningFor, long regionId, long partyID, int termNumber,
                             String personalInfo, String manifesto, String picPath, long followers){
        _id = id;
        this.name = name;
        this.DOB = DOB;
        this.POB = POB;
        this.currentJob = currentJob;
        this.runningFor = runningFor;
        this.regionId = regionId;
        this.partyID = partyID;
        this.termNumber = termNumber;
        this.personalInfo = personalInfo;
        this.manifesto = manifesto;
        this.picPath = picPath;
        this.followers = followers;
    }

    public CandidateContainer (Candidate candidate){
        _id = candidate.getId();
        this.name = candidate.getName();
        this.DOB = candidate.getDob();
        this.POB = candidate.getPob();
        this.currentJob = candidate.getCurrentJob();
        this.runningFor = candidate.getRunningFor();
        this.regionId = candidate.getRegionId();
        this.partyID = candidate.getPartyId();
        this.termNumber = candidate.getTermNumber();
        this.personalInfo = candidate.getPersonalInfo();
        this.manifesto = candidate.getManifesto();
        this.picPath = candidate.getPicPath();
        this.followers = candidate.getFollowers();
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDOB() {
        return DOB;
    }

    public void setDOB(long DOB) {
        this.DOB = DOB;
    }

    public String getPOB() {
        return POB;
    }

    public void setPOB(String POB) {
        this.POB = POB;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public long getRunningFor() {
        return runningFor;
    }

    public void setRunningFor(long runningFor) {
        this.runningFor = runningFor;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public long getPartyID() {
        return partyID;
    }

    public void setPartyID(long partyID) {
        this.partyID = partyID;
    }

    public int getTermNumber() {
        return termNumber;
    }

    public void setTermNumber(int termNumber) {
        this.termNumber = termNumber;
    }

    public String getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(String personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getManifesto() {
        return manifesto;
    }

    public void setManifesto(String manifesto) {
        this.manifesto = manifesto;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public long getFollowers() {
        return followers;
    }

    /**
     * Converts list of {@link za.ac.myuct.klmedu001.ivn.api.entity.candidateApi.model.Candidate} to a
     * list of {@code CandidateContainer} which can be used to save data locally to device using
     * DBFlow
     * @param list list of {@code Candidate} which is to be converted
     * @return converted list
     */
    public static List<CandidateContainer> fromCandidateList(List<Candidate> list){
        //Minimize risk of throwing errors due to null lists
        if(list == null)
            return new ArrayList<>(0);

        List<CandidateContainer> containerList = new ArrayList<>(list.size());

        for (Candidate candidateType : list) {
            containerList.add(new CandidateContainer(candidateType));
        }
        return containerList;
    }
}
